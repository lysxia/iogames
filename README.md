# Games

The main definitions are in `src/Game.v` and `src/Core.v`.

A game `g` consists of a type of "next moves" `move g`,
and for each possible move `m : move g`,
a game `play g m` for "the rest of the game".
Note that this representation does not care about who the players are.

```coq
CoInductive Game : Type :=
  { move : Type
  ; play : move -> Game
  }.
```

For our purposes games will implicitly be played alternatively by
a Player and an Opponent. In most definitions, Opponent plays first.

A strategy for the Player answers every Opponent move with some counter-move.
A strategy is allowed to diverge/rage-quit, leaving the game forever in suspense
Nontermination is essential to be able to compose strategies (using `apply`).

```coq
CoInductive Strategy (g : Game) : Type :=
  { coplay : forall (m : move g), Delay
      { cm : move (play g m)
      & Strategy (play (play g m) cm) } }.
```

A tensor product "`*`" and an arrow game "`*>`" (which is to be thought of as
"`-o`" in linear logic) are defined mutually recursively; see below.

Intuitively, in the game `g1 * g2`, Opponent chooses to play some move `m` on
`g1` or `g2`, and Player must respond on the same side (`play g1 m` or `play g2 m`).
In the game `g1 *> g2`, Opponent plays some move `m` on `g2`, and Player may then
play on `g1` or respond on `g2`; in other words, Player now plays `g1 * play g2 m`
as Opponent (first move).

```
move (g1 * g2) = move g1 + move g2
play (g1 * g2) (inl m1) = (play g1 m1 <* g2)
play (g1 * g2) (inr m2) = (g1 *> play g2 m2)

move (g1 *> g2) = move g2
play (g1 *> g2) m2 = (g1 * play g2 m2)

(g1 <* g2) = (g2 *> g1)
```

We get a little zoo of combinators, all expressible as (total) strategies,
which can then be interpreted as Coq functions using `apply`.

```
copycat : Strategy (g *> g)
mapO : Strategy ((g2 *> g3) *> (g1 *> g2) *> (g1 *> g3))
eval : Strategy ((g1 * (g1 *> g2)) *> g2)
bimapIO : Strategy (((g1 *> h1) * (g2 *> h2)) *> (g1 * g2) *> (h1 * h2))
curry : Strategy ((g1 * g2 *> g3) *> (g1 *> g2 *> g3))
uncurry : Strategy ((g1 *> g2 *> g3) *> (g1 * g2 *> g3))

apply : Strategy (g1 *> g2) -> Strategy g1 -> Strategy g2
```

There's an exponential modality ("of course!" in linear logic; named `Mux` in code, see `src/Mux.v`).
The game `!g` spawns copies of `g`.

```
move (!g) = unit  (* Spawn *)
play (!g) tt = !g * g
```

```
pureStrategy : Strategy g -> Strategy (!g)
copyMux : Strategy (!g *> (!g * !g))
deMux : Strategy (!g *> g)
apMux : Strategy (!(g1 *> g2) *> !g1 *> !g2)
mux : Strategy (!(g *> (g * g)) *> g *> !g)   (* If you can duplicate g, then you can have as many g as you want. *)
```

And a fixpoint operator (see `src/Fix.v`).

```
fix_ : Strategy (!(g *> g) *> g)
```

---

Main files:

- `Game.v`: Type definitions for games and strategies.
- `Core.v`: Core games and strategies (tensor product, arrow (lollipop), mapping via simulations).
- `Elementary.v`: More building blocks (empty game, sum, etc.).
- `Mux.v`: Exponential modality `!g` (`Mux g`).
- `Fix.v`: Fixpoint operator (`fix_`).
- `SemiStrategy.v`: Generalized corecursion principle for constructing strategies.
- `IOGames.v`: Toplevel file, reexporting everything in one place.
- `Extra.v`: Ongoing experiments.

Auxiliary:

- `Delay.v`: The delay monad.
- `Config.v`: Global settings (implicit arguments, primitive projections).
- `Util.v`: Miscellaneous lemmas (lists, arithmetic...).
