(** * The delay monad *)

From Coq Require Import Setoid Morphisms.

From IOGames Require Import Config.

Inductive delayed (A X : Type) : Type :=
| Stop (_ : A)
| Step (_ : X)
.

CoInductive delay (A : Type) : Type := Later
  { await : delayed A (delay A) }.

Add Printing Constructor delay.

Arguments Stop _ _ &.
Arguments Step _ _ &.
Arguments Later _ &.

(** [stop : A -> delay A] *)
(** The identity/return of the [delay] monad. *)
Notation stop x := (Later (Stop x)).

Notation "'stop@@' x" := (Later (Stop x)) (at level 200, only parsing).

Definition awaiting_ {A B} (k : A -> delayed B (delay B)) : delay A -> delay B :=
  cofix awaiting_ u := Later
    match await u with
    | Stop a => k a
    | Step u => Step (awaiting_ u)
    end.

(** The multiplication/bind of the [delay] monad. *)
Definition awaiting {A B} (u : delay A) (k : A -> delayed B (delay B)) : delay B :=
  awaiting_ k u.

Arguments awaiting A B &.

(** * Relations *)

Definition terminating {A} (d : delay A) : Prop :=
  Acc (fun y x => Step y = await x) d.

Lemma terminating_Stop {A} (x : A) : terminating (stop x).
Proof.
  constructor; discriminate.
Qed.

Lemma terminating_Step {A} (d : delay A) : terminating d -> terminating (Later (Step d)).
Proof.
  constructor; intros *; injection 1; intros ->; assumption.
Qed.

Lemma terminating_Step_inv {A} (d : delay A) : terminating (Later (Step d)) -> terminating d.
Proof.
  intros [H]. exact (H _ eq_refl).
Qed.

Lemma coterminating_Stop {A B} (x : A) (y : B)
  : terminating (stop x) <-> terminating (stop y).
Proof.
  constructor; exact (fun _ => terminating_Stop).
Qed.

Lemma terminating_eta_inv {A} (d : delay A)
  : terminating d -> terminating (Later (await d)).
Proof.
  intros H; constructor; apply H.
Defined.

Lemma terminating_eta {A} (d : delay A)
  : terminating (Later (await d)) -> terminating d.
Proof.
  intros H; constructor; apply H.
Defined.

Lemma coterminating_eta {A} (d : delay A)
  : terminating d <-> terminating (Later (await d)).
Proof.
  constructor.
  - apply terminating_eta_inv.
  - apply terminating_eta.
Qed.

Lemma coterminating_Step {A} (d : delay A)
  : terminating d <->
      match await d with
      | Stop _ => True
      | Step y => terminating y
      end.
Proof.
  constructor.
  - intros [H]; destruct (await d); [exact I| exact (H _ eq_refl)].
  - constructor; destruct (await d); [discriminate | intros ?; injection 1; intros ->; assumption].
Qed.

Definition force_ {A} (force : forall d (t : terminating d), A)
    (d : delayed A (delay A)) (t : forall d', Step d' = d -> terminating d')
  : A :=
  match d as y return y = _ -> _ with
  | Stop x => fun _ => x
  | Step y => fun p => force _ (t _ p)
  end eq_refl.

Fixpoint force {A} (d : delay A) (t : terminating d) {struct t} : A :=
  force_ force (Acc_inv t).

Arguments force : clear implicits.
Arguments force {A}.

Arguments force_ : clear implicits.
Arguments force_ {A}.

Lemma force_unfold {A} (d : delay A) (t : terminating d)
  : force _ t = force_ force _ (Acc_inv t).
Proof.
  destruct t; reflexivity.
Qed.

Lemma force_fold {A} (d : delay A) (t : _)
  : force_ force (await d) t = force _ (Acc_intro _ t).
Proof.
  reflexivity.
Qed.

Lemma force_eta_inv {A} (d : delay A) (t : terminating (Later (await d)))
  : force (Later (await d)) t = force d (terminating_eta t).
Proof.
  destruct t; reflexivity.
Qed.

Lemma force_irrel {A} (d : delay A)
  : forall (t t' : terminating d), force _ t = force _ t'.
Proof.
  intros t; generalize t. induction t; intros [] [].
  rewrite 2 force_unfold; cbn.
  destruct (await x); cbn; [reflexivity | exact (H0 _ eq_refl _ _)].
Defined.

Record value {A} (d : delay A) (a : A) : Prop := Mk_terminating_at
  { terminating_value : terminating d
  ; force_value : force d terminating_value = a
  }.

Infix "!!" := value (at level 70, no associativity).

Ltac abstract_terminating :=
  repeat match goal with
  | [ |- context [force _ ?t] ] => tryif is_var t then fail else (generalize t; intros ?)
  | [ |- context [force_ _ _ ?t]] => tryif is_var t then fail else (generalize t; intros ?)
  end.

Lemma value_ind {A} a (P : delay A -> Prop)
    (IND : forall d,
       match await d with
       | Step d' => P d'
       | Stop a' => a = a'
       end -> P d)
  : forall d, d !! a -> P d.
Proof.
  intros d [T e]; revert e; generalize T; induction T; intros T'.
  rewrite force_unfold. abstract_terminating.
  specialize (IND x).
  destruct (await x); cbn; intros <-; apply IND.
  - reflexivity.
  - exact (H0 _ eq_refl _ eq_refl).
Qed.

Lemma value_stop {A} (a : A) : stop a !! a.
Proof.
  refine {| terminating_value := terminating_Stop ; force_value := _ |}.
  rewrite force_unfold; reflexivity.
Qed.

Lemma value_stop_inv {A} (a a' : A) : stop a !! a' -> a = a'.
Proof.
  intros [t v]. rewrite force_unfold in v; apply v.
Qed.

Lemma value_unique {A} (d : delay A) (a a' : A) : d !! a -> d !! a' -> a = a'.
Proof.
  intros [t v] [t' v']; rewrite <- v, <- v'; apply force_irrel.
Qed.

Record delay_rel {A B} (r : A -> B -> Type) (d1 : delay A) (d2 : delay B) : Type :=
  { coterminating : terminating d1 <-> terminating d2
  ; eq_value : forall a b, d1 !! a -> d2 !! b -> r a b
  }.

Definition stop_rel {A B} {r : A -> B -> Type} (x : A) (y : B)
  : r x y -> delay_rel r (stop x) (stop y).
Proof.
  constructor.
  - exact coterminating_Stop.
  - intros a b Va Vb; apply value_stop_inv in Va, Vb; destruct Va, Vb; assumption.
Defined.

Arguments stop_rel _ _ _ _ _ &.

Lemma terminating_awaiting_inv {A B} (d : delay A) (k : A -> delayed B (delay B))
  : (forall a, terminating (Later (k a))) ->
    terminating (awaiting d k) -> terminating d.
Proof.
  intros H H0. constructor.
  remember (awaiting d k) as adk; revert d Heqadk. induction H0 as [d0 H1 H2].
  intros d ->; cbn in H1, H2; destruct (await d).
  + discriminate.
  + intros ?; injection 1; intros ->. constructor. exact (H2 _ eq_refl _ eq_refl).
Defined.

Lemma coterminating_awaiting {A B} (d : delay A) (k : A -> delayed B (delay B))
  : (forall a, terminating (Later (k a))) ->
    terminating (awaiting d k) <-> terminating d.
Proof.
  intros H; split.
  - apply (terminating_awaiting_inv _ H).
  - intros H0; induction H0 as [d H1 H2]; constructor; cbn; destruct (await d); intros.
    + apply H in H0; exact H0.
    + injection H0; intros ->; apply (H2 _ eq_refl).
Qed.

Lemma force_awaiting {A B} (d : delay A) (k : A -> delayed B (delay B))
    (tk : forall a, terminating (Later (k a))) (tdk : terminating (awaiting d k))
  : force _ tdk = force (Later (k (force d (terminating_awaiting_inv _ tk tdk)))) (tk _).
Proof.
  generalize (terminating_awaiting_inv _ tk tdk). intros t; generalize t.
  revert tdk. induction t; intros.
  rewrite (force_unfold (d := awaiting x _)).
  rewrite (force_unfold (d := x)).
  destruct tdk, t; cbn in *.
  destruct (await x); cbn.
  - rewrite force_fold with (d := Later (k a1)). apply (force_irrel (d := Later (k a1))).
  - apply (H0 _ eq_refl).
Qed.

Definition eq_delay {A} (d1 d2 : delay A) : Prop := inhabited (delay_rel eq d1 d2).

Declare Scope delay_scope.
Delimit Scope delay_scope with delay.

Infix "=" := eq_delay : delay_scope.

Lemma value_mon {A} (d1 d2 : delay A) (a : A)
  : (terminating d1 -> terminating d2) -> d1 !! a -> exists b, d2 !! b.
Proof.
  intros F [T V]. exists (force d2 (F T)).
  econstructor; reflexivity.
Qed.

Lemma value_eq {A} (d1 d2 : delay A) (a : A) : (d1 = d2)%delay -> d1 !! a -> d2 !! a.
Proof.
  intros [[T E]] V.
  destruct (value_mon (proj1 T) V) as [b Vb].
  rewrite (E _ _ V Vb). assumption.
Qed.

#[local] Instance Reflexive_eq_delay {A} : Reflexive (A := delay A) eq_delay.
Proof.
  constructor; constructor.
  - reflexivity.
  - apply value_unique.
Qed.

#[local] Instance Symmetric_eq_delay {A} : Symmetric (A := delay A) eq_delay.
Proof.
  constructor; constructor.
  - symmetry; destruct H as [H]; apply H.
  - symmetry; destruct H as [H]; apply H; assumption.
Qed.

#[local] Instance Transitive_eq_delay {A} : Transitive (A := delay A) eq_delay.
Proof.
  intros x y z Hx Hy; constructor; constructor.
  - etransitivity; [ destruct Hx as [Hx]; apply Hx | destruct Hy as [Hy]; apply Hy ].
  - intros t1 t2 V1.
    apply (value_eq Hx) in V1. apply (value_eq Hy) in V1.
    revert V1; apply value_unique.
Qed.

#[export] Instance Equivalence_eq_delay {A} : Equivalence (A := delay A) eq_delay.
Proof.
  constructor; typeclasses eauto.
Qed.

#[export] Instance Proper_value {A} : Proper (eq_delay ==> eq ==> iff) (value (A := A)).
Proof.
  unfold Proper, respectful; intros ? ? H ? _ <-.
  split; apply value_eq; [assumption | symmetry; assumption ].
Qed.

Lemma eq_value_force {A} (d1 d2 : delay A)
  : (forall t1 t2, force d1 t1 = force d2 t2) -> (forall a b, d1 !! a -> d2 !! b -> a = b).
Proof.
  intros H a b [V1 <-] [V2 <-]. apply H.
Qed.

Lemma eq_eta {A} (d : delay A) : (d = Later (await d))%delay.
Proof.
  constructor; constructor.
  - apply coterminating_eta.
  - apply eq_value_force; intros; rewrite force_eta_inv. apply force_irrel.
Qed.

Lemma terminating_eq {A} (d1 d2 : delay A)
  : (d1 = d2)%delay -> terminating d1 -> terminating d2.
Proof.
  intros [H]; apply H.
Qed.

Lemma coterminating_awaiting_k {A B} (d : delay A) (k : A -> delayed B (delay B))
  : forall a, d !! a ->
      (terminating (awaiting d k) <-> terminating (Later (k a))).
Proof.
  intros a V. apply value_ind with (a := a) (d := d); [ | assumption ].
  intros d0; rewrite (coterminating_eta (d := awaiting _ _)); cbn.
  destruct (await d0); [ intros <-; reflexivity | ].
  intros <-; rewrite coterminating_Step; reflexivity.
Qed.

Lemma eq_Step {A} (d : delay A)
  : (Later (Step d) = d)%delay.
Proof.
  constructor; constructor.
  - apply (coterminating_Step (d := Later (Step d))).
  - apply eq_value_force; intros; rewrite force_unfold; cbn; apply force_irrel.
Qed.

Lemma eq_awaiting {A B} (d : delay A) a (H : d !! a) (k : A -> delayed B (delay B))
  : (awaiting d k = Later (k a))%delay.
Proof.
  apply value_ind with (a := a) (d := d); [ | assumption ].
  intros d' E. rewrite eq_eta; cbn; destruct (await d').
  - destruct E; reflexivity.
  - rewrite eq_Step; assumption.
Qed.
