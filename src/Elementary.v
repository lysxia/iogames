From IOGames Require Import
  Config Util Delay Game Core Clone.

#[local] Open Scope game_scope.

(** * Basic games *)

(** The empty game.
    The continuation [g], which is unique extensionally, is allowed to vary intensionally. *)
Definition Zero_ (g : False -> Game) :=
  {| move := False ; play := g |}.

(** The empty game. *)
Definition Zero := Zero_ (fun v : False => match v with end).

(** The empty strategy. *)
Notation zero_ := (totalplaying v => match v with end).

Definition zero {g} : TotalStrategy (Zero_ g) := zero_.

(** Extend a game with half a round, swapping Player and Opponent. *)
Definition Half (A : Type) (g : Game) : Game :=
  {| move := A ; play := fun _ => g |}.

Definition Ack (g : Game) : Game :=
  {| move := unit ; play := fun _ => g |}.

Definition ack {g : Game} (s : CoStrategy g) : Strategy (Ack g) :=
  coplaying _ => s.

Definition ackS {g1 g2} (z : Simulation g1 g2) : Simulation (Ack g2) (Ack g1) :=
  fun _ => (tt ?? z)%sim.

(** Extend a game with one round where Opponent plays a trivial move [tt]
    and Player is to play an [A]. *)
Definition Succ (A : Type) (g : Game) : Game :=
  Ack (Half A g).

(** A game with only one round. *)
Definition One A := Succ A Zero.

Definition succ {A g} (x : A) (s : Strategy g) : Strategy (Succ A g) :=
  coplaying _ => stop (x ?? s).

Definition succT {A g} (x : A) (s : TotalStrategy g) : TotalStrategy (Succ A g) :=
  totalplaying _ => (x ?? s).

(** A strategy for [One A] consists of just one move [x : A]. *)
Definition one {A} (x : A) : TotalStrategy (One A) := succT x zero.

Definition seq {g} : TotalStrategy (One unit * g *> g) :=
  totalplaying m => inl (inl tt) ??
  totalplaying _ => inl (inr m) ??
  totalplaying cm => inr cm ?? sndIO.

Definition seqWith {A B C} (f : A -> B -> C) : TotalStrategy (One A * One B *> One C) :=
  totalplaying _ => inl (inl tt) ??
  totalplaying x => inl (inr tt) ??
  totalplaying y => inr (f x y) ?? zero_.

(* TODO: To make this a TotalStrategy, get rid of pairwith2 *)
Definition cloneOne {A} : Strategy (One A *> One A * One A) :=
  coplaying m => stop
    match m with
    | inl _ => inl tt ?? coplaying x => stop@@
               inr x ?? pairwith2 (one x)
    | inr _ => inl tt ?? coplaying x => stop@@
               inr x ?? pairwith1 (one x)
    end.

(** * Static games *)

(* Circles back to the initial state after one player-opponent round. *)
CoFixpoint Static (ms : Type) (cms : ms -> Type) : Game :=
  {| move := ms
  ;  play := fun m =>
     {| move := cms m
     ;  play := fun _ => Static cms
     |}
  |}.

Definition Static' (ms : Type) (cms : ms -> Type) (m : ms) : Game :=
  {| move := cms m
  ;  play := fun _ => Static cms
  |}.

(* This can be used for a linear calculus with duplicable base types (including
   mutable references) *)
CoFixpoint cloneStatic {ms} {cms : ms -> Type} : TotalStrategy (Clone (Static cms)) :=
  totalplaying '(inl m | inr m) => inl m ??
  totalplaying cm => inr cm ?? cloneStatic.

(** * Sum *)

Definition Choose (g1 g2 : Game) : Game :=
  {| move := bool
  ;  play := fun b => if b then g1 else g2
  |}.

Definition Sum (g1 g2 : Game) : Game := Ack (Choose g1 g2).

Infix "+" := Sum : game_scope.

Definition sum_left {g1 g2} : TotalStrategy (g1 *> (g1 + g2)) :=
  totalplaying _ => (inr true ?? copycatO).

Definition sum_right {g1 g2} : TotalStrategy (g2 *> (g1 + g2)) :=
  totalplaying _ => (inr false ?? copycatO).

Definition map_sum {g1 h1 g2 h2}
  : TotalStrategy ((g1 *> h1) * (g2 *> h2) *> (g1 + g2) *> (h1 + h2)) :=
  totalplaying _ => inr (inl tt) ??
  totalplaying cm => inr (inr cm) ??
  match cm with
  | true => fstIO
  | false => sndIO
  end.

Definition case_sum {g1 g2 h} : TotalStrategy (((g1 *> h) * (g2 *> h)) *> (g1 + g2) *> h) :=
  totalplaying m => inr (inl tt) ??
  totalplaying cm =>
    match cm with
    | true => totalplay (fstIO (g1 := g1 *> h)) m
    | false => totalplay (sndIO (g2 := g2 *> h)) m
    end.

(** * Stream *)

CoFixpoint Stream (A : Type) : Game := Ack (CoStream A)
with CoStream (A : Type) :=
  {| move := A ; play := fun _ => Stream A |}.

CoFixpoint nats_from (n : nat) : TotalStrategy (Stream nat) :=
  totalplaying _m => (n ?? nats_from (S n)).

Definition nats : TotalStrategy (Stream nat) := nats_from 0.

CoFixpoint cloneCoStream {A} : Simulation (CoStream A) (CoStream A * CoStream A) := fun m =>
  match m with
  | inl m | inr m => m ?? fun _ => tt ?? cloneCoStream
  end%sim.

(* TODO: This is total, but *not* a simulation because of sum_left/sum_right *)
Definition cloneSum1_ {g1 g2} : Strategy (Clone g1 * Clone g2 *> g1 *> (g1 + g2) * g1) :=
  fstIO >>> apply dimapO (pairIO copycatO (apply bimapIO (pairIO sum_left copycatO))).

Definition cloneSum2_ {g1 g2} : Strategy (Clone g1 * Clone g2 *> g2 *> (g1 + g2) * g2) :=
  sndIO >>> apply dimapO (pairIO copycatO (apply bimapIO (pairIO sum_right copycatO))).

Definition cloneSum {g1 g2} : Strategy (Clone g1 * Clone g2 *> Clone (g1 + g2)) :=
  coplaying m => stop
    match m with
    | inl m =>
        inr (inl m) ?? coplaying cm => stop@@ inr (inr cm) ??
          match cm with
          | true  => cloneSum1_ >>> apply dimapO (pairIO copycatO swap)
          | false => cloneSum2_ >>> apply dimapO (pairIO copycatO swap)
          end
    | inr m =>
        inr (inl m) ?? coplaying cm => stop@@ inr (inr cm) ??
          match cm with
          | true => cloneSum1_
          | false => cloneSum2_
          end
    end.

Definition cloneZero : TotalStrategy (Clone Zero) :=
  totalplaying (inl m | inr m) => match m with end.

(** * State *)

Module State.

Inductive state_move (S : Type) : Type :=
| Get
| Put (s : S)
.

Definition state_comove (S : Type) (m : state_move S) : Type :=
  match m with
  | Get => S
  | Put _ => unit
  end.

Definition g (S : Type) : Game := Static (@state_comove S).

CoFixpoint run {S : Type} (s : S) : TotalStrategy (State.g S) :=
  totalplaying m =>
    match m with
    | Get => s ?? run s
    | Put s => tt ?? run s
    end.

Definition get {S : Type} : Simulation (State.g S) (One S) :=
  fun _ => (Get ?? fun s => s ?? fun v => match v with end)%sim.

Definition put {S : Type} : Simulation (State.g S) (Half S (Ack Zero)) :=
  fun s => (Put s ?? fun _ => tt ?? fun v => match v with end)%sim.

End State.
