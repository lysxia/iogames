(** * Core definitions *)

(** - Pairs and arrows
    - Basic simulations
    - Embedding of total games
 *)

From IOGames Require Import
  Config Util Delay Game.

#[local] Open Scope game_scope.

(** * Tensor product and functions *)

(** Tensor product: every round, Opponent choosees to play on [gi] or [go],
    and Player must respond on the corresponding board. *)
CoFixpoint IOGame (gi go : Game) : Game :=
  {| move := move gi + move go
  ;  play := fun m =>
       match m with
       | inl mi => IGame (play gi mi) go
       | inr mo => OGame gi (play go mo)
       end |}
(** (Right) arrow: Opponent plays [m] on [go], and Player may challenge opponent on [gi],
    for as long as Player wants (possibly zero times, possibly forever),
    before responding on [go] (i.e., Player plays [gi * (play go m)] as Opponent). *)
with OGame (gi go : Game) : Game :=
  {| move := move go
  ;  play := fun mo => IOGame gi (play go mo) |}
(** (Left) arrow: [IGame gi go] is equivalent to [OGame go gi]. *)
with IGame (gi go : Game) : Game :=
  {| move := move gi
  ;  play := fun mi => IOGame (play gi mi) go |}.

(** We could define [IGame] to be equal [OGame], but then their arguments would keep flipping
    around, which is likely to be confusing. *)

(* Internal hom ([OGame gi go] is isomorphic to [IGame go gi], representing functions [gi -o go]) *)

Infix "*" := IOGame : game_scope.
Infix "<*" := IGame (at level 91, left associativity) : game_scope.
Infix "*>" := OGame (at level 92, right associativity) : game_scope.

(* [IOGame] (or [_ * _]) is not a Cartesian product: we cannot define
   [A *> (A * A)] in general. However, the subcategory of games equipped with such [clone]
   operators is closed under many common operators found in this library. *)

(** * Total strategies *)

Notation "'coplaying' m '=>' s" := {| coplay := fun m => s |}
  (at level 200, m pattern, s at level 200).

Notation "'coplaying' ' m '=>' s" := {| coplay := fun pat => match pat with m => s end |}
  (at level 200, m pattern, s at level 200).

Notation "'totalplaying' m '=>' s" := {| totalplay := fun m => s |}
  (at level 200, m pattern, s at level 200).

Notation "'totalplaying' ' m '=>' s" := {| totalplay := fun pat => match pat with m => s end |}
  (at level 200, m pattern, s at level 200).

CoFixpoint partial {g} (s : TotalStrategy g) : Strategy g :=
  coplaying m => let '(cm ?? s) := totalplay s m in stop
    (cm ?? partial s).

Coercion partial : TotalStrategy >-> Strategy.

(** * Simulations *)

CoFixpoint mapSimulation {g1 g2} (z : Simulation g1 g2) : TotalStrategy (g1 *> g2) :=
  totalplaying m2 => let '(m1 ?? z)%sim := z m2 in
       (inl m1 ?? comapSimulation z)
with comapSimulation {g1 g2} (z : Simulation g1 g2) : TotalStrategy (g2 <* g1) :=
  totalplaying m1 => let '(m2 ?? z)%sim := z m1 in
       (inr m2 ?? mapSimulation z).

Coercion mapSimulation : Simulation >-> TotalStrategy.

CoFixpoint idSimulation {g} : Simulation g g :=
  fun m => (m ?? idSimulation)%sim.

CoFixpoint catSimulation {g1 g2 g3} (s1 : Simulation g1 g2) (s2 : Simulation g2 g3) : Simulation g1 g3 :=
  fun m3 => (let '(m2 ?? s2) := s2 m3 in let '(m1 ?? s1) := s1 m2 in
    m1 ?? catSimulation s2 s1)%sim.

Infix ">>>" := catSimulation (at level 70) : sim_scope.

CoFixpoint applySim {g1 g2} (s : Simulation g1 g2) (s1 : Strategy g1) : Strategy g2 :=
  coplaying m2 => let '(m1 ?? s)%sim := s m2 in
    awaiting (coplay s1 m1) (fun '(cm1 ?? s1) => let '(cm2 ?? s)%sim := s cm1 in
    Stop (cm2 ?? applySim s s1)).

CoFixpoint applySimT {g1 g2} (s : Simulation g1 g2) (s1 : TotalStrategy g1) : TotalStrategy g2 :=
  totalplaying m2 => let '(m1 ?? s)%sim := s m2 in
    let '(cm1 ?? s1) := totalplay s1 m1 in
    let '(cm2 ?? s)%sim := s cm1 in
    cm2 ?? applySimT s s1.

(** * Copycat *)

(** The copycat strategy is the identity in the category of games and strategies. *)

Definition copycat {g} : TotalStrategy (g *> g) := idSimulation.

(* Equivalent but direct definition: *)
CoFixpoint copycatO {g} : TotalStrategy (g *> g) :=
  totalplaying m => (inl m ?? copycatI)
with copycatI {g} : TotalStrategy (g <* g) :=
  totalplaying m => (inr m ?? copycatO).

CoFixpoint dimapSimO {g1 g1' g2 g2'} (s1 : Simulation g1' g1) (s2 : Simulation g2 g2')
  : Simulation (g1 *> g2) (g1' *> g2') :=
  fun m2 => let '(m2 ?? s2)%sim := s2 m2 in
    (m2 ?? bimapSimIO s1 s2)%sim
with dimapSimI {g1 g1' g2 g2'} (s1 : Simulation g1 g1') (s2 : Simulation g2' g2)
  : Simulation (g1 <* g2) (g1' <* g2') :=
  fun m1 => let '(m1 ?? s1)%sim := s1 m1 in
     (m1 ?? bimapSimIO s1 s2)%sim
with bimapSimIO {g1 g1' g2 g2'} (s1 : Simulation g1 g1') (s2 : Simulation g2 g2')
  : Simulation (g1 * g2) (g1' * g2') := fun m =>
    match m with
    | inl m1 => let '(m1 ?? s1)%sim := s1 m1 in
      (inl m1 ?? dimapSimI s1 s2)%sim
    | inr m2 => let '(m2 ?? s2)%sim := s2 m2 in
      (inr m2 ?? dimapSimO s1 s2)%sim
    end.

(** * Interaction *)

(** Strategies can interact when they play opposite roles on a game. *)

Section Apply.

Context (apply : forall g1 g2 (s2 : Strategy (g1 *> g2)) (s1 : Strategy g1), Strategy g2).

Arguments apply {g1 g2}.

CoFixpoint coapplyFun {g1 g2} (s2 : CoStrategy (g1 * g2)) (s1 : Strategy g1) : CoStrategy g2 :=
  awaiting s2 (fun s2 =>
  match next s2 as n return Strategy (play (g1 * g2) n) -> _ with
  | inl m1 => fun s2 => Step (coapplyArg s2 (coplay s1 m1))
  | inr m2 => fun s2 => Stop (m2 ?? apply s2 s1)
  end (after s2))
with coapplyArg {g1 g2} (s2 : Strategy (g1 <* g2)) (s1 : CoStrategy g1) : CoStrategy g2 :=
  awaiting s1 (fun '(m ?? s1) => Step (coapplyFun (coplay s2 m) s1)).

End Apply.

CoFixpoint apply g1 g2 (s2 : Strategy (g1 *> g2)) (s1 : Strategy g1) : Strategy g2 :=
  coplaying m2 => coapplyFun apply (coplay s2 m2) s1.

Arguments apply {g1 g2}.

(* Application as an arrow *)

CoFixpoint eval {g1 g2} : TotalStrategy ((g1 * (g1 *> g2)) *> g2) :=
  totalplaying m2 => inl (inr m2) ?? coeval
with coeval {g1 g2} : TotalStrategy ((g1 *> (g1 * g2)) <* g2) :=
  totalplaying cm =>
    match cm with
    | inl m1 =>
        inl (inl m1) ?? totalplaying cm1 =>
        inl (inr cm1) ?? coeval
    | inr cm2 => inr cm2 ?? eval
    end.

(*
    (g1  *  (g1  *> g2 )) *> g2
O>                           m2
    (g1  *  (g1  *> g2 )) *  g2'
P>                  m2
    (g1  *> (g1  *  g2')) <* g2'
O>           m1
    (g1  *  (g1' <* g2'))  * g2'
P>   m1
    (g1' <* (g1' <* g2')) <* g2'
O>   m1'
    (g1" *  (g1' <* g2'))  * g2'
P>           m1'
    (g1" *> (g1"  * g2')) <* g2'
O>                  m2'
    (g1" *  (g1" *> g2"))  * g2'
P>                           m2'
    (g1" *  (g1" *> g2")) *> g2"
*)

CoFixpoint fstIO {g1 g2} : Simulation (g1 * g2) g1 :=
  fun m => (inl m ?? fun cm => cm ?? fstIO)%sim.

CoFixpoint sndIO {g1 g2} : Simulation (g1 * g2) g2 :=
  fun m => (inr m ?? fun cm => cm ?? sndIO)%sim.

CoFixpoint swap {g1 g2} : Simulation (g1 * g2) (g2 * g1) := fun m =>
  match m with
  | inl m => inr m ?? fun cm => cm ?? swap
  | inr m => inl m ?? fun cm => cm ?? swap
  end%sim.

Definition mirrorOI {g1 g2} : Simulation (g1 *> g2) (g2 <* g1) := fun m =>
  (m ?? swap)%sim.

Definition mirrorIO {g1 g2} : Simulation (g1 <* g2) (g2 *> g1) := fun m =>
  (m ?? swap)%sim.

Definition const {g1 g2} : Simulation g1 (g2 *> g1) := fun m =>
  (m ?? sndIO)%sim.

Section Curry.

Context (curry_1_ : forall g1 g2 g3, Simulation ((g1 <* g2) <* g3) (g1 <* (g2 * g3))).
Context (curry_2_ : forall g1 g2 g3, Simulation ((g1 *> g2) <* g3) (g1 *> (g2 <* g3))).
Context (curry : forall g1 g2 g3, Simulation ((g1 * g2) *> g3) (g1 *> (g2 *> g3))).

Arguments curry_1_ {g1 g2 g3}.
Arguments curry_2_ {g1 g2 g3}.
Arguments curry {g1 g2 g3}.

Definition assoc_r_ {g1 g2 g3} : Simulation (g1 * (g2 * g3)) ((g1 * g2) * g3) :=
  fun m =>
    match m with
    | inl (inl m1) => inl m1 ?? curry_1_
    | inl (inr m2) => inr (inl m2) ?? curry_2_
    | inr m3 => inr (inr m3) ?? curry
    end%sim.

End Curry.

CoFixpoint curry {g1 g2 g3} : Simulation ((g1 * g2) *> g3) (g1 *> (g2 *> g3)) :=
  fun m => (m ?? assoc_r_ (@curry_1_) (@curry_2_) (@curry))%sim
with curry_2_ {g1 g2 g3} : _ :=
  fun m => (m ?? assoc_r_ (@curry_1_) (@curry_2_) (@curry))%sim
with curry_1_ {g1 g2 g3} : _ :=
  fun m => (m ?? assoc_r_ (@curry_1_) (@curry_2_) (@curry))%sim.

Definition assoc_r {g1 g2 g3} : Simulation (g1 * (g2 * g3)) ((g1 * g2) * g3) :=
  assoc_r_ (@curry_1_) (@curry_2_) (@curry).

Section Uncurry.

Context (uncurry_1_ : forall g1 g2 g3, Simulation (g1 <* (g2 * g3)) ((g1 <* g2) <* g3)).
Context (uncurry_2_ : forall g1 g2 g3, Simulation (g1 *> (g2 <* g3)) ((g1 *> g2) <* g3)).
Context (uncurry : forall g1 g2 g3, Simulation (g1 *> (g2 *> g3)) ((g1 * g2) *> g3)).

Arguments uncurry_1_ {g1 g2 g3}.
Arguments uncurry_2_ {g1 g2 g3}.
Arguments uncurry {g1 g2 g3}.

Definition assoc_l_ {g1 g2 g3} : Simulation ((g1 * g2) * g3) (g1 * (g2 * g3)) :=
  fun m =>
    match m with
    | inl m1 => inl (inl m1) ?? uncurry_1_
    | inr (inl m2) => inl (inr m2) ?? uncurry_2_
    | inr (inr m3) => inr m3 ?? uncurry
    end%sim.

End Uncurry.

CoFixpoint uncurry {g1 g2 g3} : Simulation (g1 *> (g2 *> g3)) ((g1 * g2) *> g3) :=
  fun m => (m ?? assoc_l_ (@uncurry_1_) (@uncurry_2_) (@uncurry))%sim
with uncurry_2_ {g1 g2 g3} : _ :=
  fun m => (m ?? assoc_l_ (@uncurry_1_) (@uncurry_2_) (@uncurry))%sim
with uncurry_1_ {g1 g2 g3} : _ :=
  fun m => (m ?? assoc_l_ (@uncurry_1_) (@uncurry_2_) (@uncurry))%sim.

Definition assoc_l {g1 g2 g3} : Simulation ((g1 * g2) * g3) (g1 * (g2 * g3)) :=
  assoc_l_ (@uncurry_1_) (@uncurry_2_) (@uncurry).

(** The functor [fun g2 => Hom(g1, g2)] *)
CoFixpoint mapO {g1 g2 g3} : TotalStrategy ((g2 *> g3) *> (g1 *> g2) *> (g1 *> g3)) :=
  totalplaying m3 => inl m3 ?? mapO_
with mapO_ {g1 g2 g3} : TotalStrategy ((g2 * g3) <* (g1 *> g2) * (g1 * g3)) :=
  totalplaying m =>
       match m with
       | inl m2 => inr (inl m2) ?? mapO2_
       | inr m3 => inr (inr (inr m3)) ?? mapO
       end
with mapO2_ {g1 g2 g3} : TotalStrategy ((g2 <* g3) *> ((g1 * g2) <* (g1 * g3))) :=
  totalplaying m =>
       match m with
       | inl m1 => inr (inr (inl m1)) ?? mapI
       | inr m2 => inl m2 ?? mapO_
       end
with mapI {g1 g2 g3} : TotalStrategy ((g2 <* g3) *> (g1 <* g2) *> (g1 <* g3)) :=
  totalplaying m1 => inr (inl m1) ?? mapO2_.

(** The composition operation in the category of games:
    two strategies [s1 : Strategy (g1 *> g2)] and [s2 : Strategy (g2 *> g3)] interact on [g2],
    yielding a strategy where [g2] is hidden, exposing the combined behavior of
    [s1] and [s2] on [g1] and [g3]. *)

Definition Ocompose g1 g2 g3 (s1 : Strategy (g1 *> g2)) (s2 : Strategy (g2 *> g3))
  : Strategy (g1 *> g3) :=
  apply (apply mapO s2) s1.

Definition Icompose g1 g2 g3 (s1 : Strategy (g1 <* g2)) (s2 : Strategy (g2 <* g3))
  : Strategy (g1 <* g3) :=
  apply (apply mapI s2) s1.

Arguments Ocompose {g1 g2 g3}.
Arguments Icompose {g1 g2 g3}.

Infix ">>>" := Ocompose : game_scope.


CoFixpoint pairIO {g1 g2} (s1 : Strategy g1) (s2 : Strategy g2) : Strategy (g1 * g2) :=
  coplaying m =>
       match m with
       | inl m1 => awaiting (coplay s1 m1) (fun '(m ?? s1) => Stop (m ?? pairIO s1 s2))
       | inr m2 => awaiting (coplay s2 m2) (fun '(m ?? s2) => Stop (m ?? pairIO s1 s2))
       end.

CoFixpoint bimapIO {g1 g2 h1 h2}
  : Simulation ((g1 *> h1) * (g2 *> h2)) ((g1 * g2) *> (h1 * h2)) := fun mh =>
  match mh with
  | inl mh1 => inl mh1 ?? bimapIO1_
  | inr mh2 => inr mh2 ?? bimapIO2_
  end%sim
with bimapIO1_ {g1 g2 h1 h2}
  : Simulation ((g1 * g2) * (h1 <* h2)) ((g1 * h1) <* (g2 *> h2)) := fun m1 =>
  match m1 with
  | inl mg1 => inl (inl mg1) ?? mapI_
  | inr mh1 => inr mh1 ?? bimapIO
  end%sim
with bimapIO2_ {g1 g2 h1 h2}
  : Simulation ((g1 * g2) * (h1 *> h2)) ((g1 *> h1) *> (g2 * h2)) := fun m2 =>
  match m2 with
  | inl mg2 => inl (inr mg2) ?? dimapO_
  | inr mh2 => inr mh2 ?? bimapIO
  end%sim
with mapI_ {g1 g2 h1 h2} : Simulation ((g1 <* h1) * (g2 *> h2)) ((g1 <* g2) <* (h1 <* h2)) :=
  fun mg1 => (inl mg1 ?? bimapIO1_)%sim
with dimapO_ {g1 g2 h1 h2} : Simulation ((g1 *> h1) * (g2 <* h2)) ((g1 *> g2) <* (h1 *> h2)) :=
  fun mg2 => (inr mg2 ?? bimapIO2_)%sim.

Definition dimapO {g1 g2 h1 h2} : Simulation ((h1 *> g1) * (g2 *> h2)) ((g1 *> g2) *> (h1 *> h2)) :=
  (bimapSimIO idSimulation mirrorOI >>> dimapO_ >>> mirrorIO)%sim.

Definition pairwith1 {g1 g2} (s : Strategy g1) : Strategy (g2 *> (g1 * g2)) :=
  apply (applySimT curry copycatO) s.

Definition pairwith2 {g1 g2} (s : Strategy g2) : Strategy (g1 *> (g1 * g2)) :=
  (pairwith1 s >>> swap)%game.

Section Transpose22.

#[local] Open Scope sim_scope.

CoFixpoint transpose22 {g1 h1 g2 h2} : Simulation ((g1 * h1) * (g2 * h2)) ((g1 * g2) * (h1 * h2)) :=
   fun m =>
     match m with
     | inl (inl mg1) => inl (inl mg1) ?? fun cm => cm ?? transpose22
     | inl (inr mh1) => inr (inl mh1) ?? fun cm => cm ?? transpose22
     | inr (inl mg2) => inl (inr mg2) ?? fun cm => cm ?? transpose22
     | inr (inr mh2) => inr (inr mh2) ?? fun cm => cm ?? transpose22
     end.

End Transpose22.
