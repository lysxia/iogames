From Coq Require Import List.
Import ListNotations.

From IOGames Require Import Config.

Arguments inl _ _ &.
Arguments inr _ _ &.
Arguments exist _ &.

Notation "f @@ x" := (f x) (at level 100, only parsing).

Ltac printgoal := match goal with |- ?G => idtac G end.

CoInductive stream (A : Type) : Type := Stream
  { head : A
  ; tail : stream A
  }.

Fixpoint inbounds {A} (xs : list A) (n : nat) {struct xs} : Prop :=
  match xs, n with
  | [], _ => False
  | _ :: _, O => True
  | _ :: xs, S n => inbounds xs n
  end.

Fixpoint get {A} {xs : list A} {n : nat} : inbounds xs n -> A :=
  match xs, n return inbounds xs n -> A with
  | [], _ => fun p => match p in False with end
  | x :: _, O => fun _ => x
  | _ :: xs, S n => fun p => get (xs := xs) p
  end.

Fixpoint before {A} {xs : list A} {n : nat} : inbounds xs n -> list A :=
  match xs, n with
  | [], _ => fun p => match p with end
  | _ :: _, O => fun _ => nil
  | x :: xs, S _ => fun p => x :: before (xs := xs) p
  end.

Fixpoint afterl {A} {xs : list A} {n : nat} : inbounds xs n -> list A :=
  match xs, n with
  | [], _ => fun p => match p with end
  | _ :: xs, O => fun _ => xs
  | _ :: xs, S _ => fun p => afterl (xs := xs) p
  end.

Definition first {A} (xs : list A) (x' : A) : A :=
  match xs with
  | [] => x'
  | x :: _ => x
  end.

Fixpoint last {A} (x0 : A) (xs : list A) : A :=
  match xs with
  | [] => x0
  | x1 :: xs => last x1 xs
  end.

Fixpoint init {A} (x0 : A) (xs : list A) : list A :=
  match xs with
  | [] => []
  | x :: xs => x0 :: init x xs
  end.

Fixpoint weave {A} (f : A -> A -> A) (x0 : A) (xs : list A) : list A :=
  match xs with
  | [] => []
  | x1 :: xs => f x0 x1 :: weave f x1 xs
  end.

Fixpoint inbounds_app {A} {n : nat} {xs} (ys : list A) : inbounds xs n -> inbounds (xs ++ ys) n :=
  match xs, n with
  | [], _ => fun p => match p with end
  | _ :: _, O => fun p => p
  | _ :: xs, S _ => fun p => inbounds_app (xs := xs) ys p
  end.

Arguments inbounds_app _ _ &.

Fixpoint inbounds_app_r {A} {n : nat} xs {ys : list A} : inbounds ys n -> inbounds (xs ++ ys) (length xs + n) :=
  match xs with
  | [] => fun p => p
  | _ :: xs => fun p => inbounds_app_r xs p
  end.

Arguments inbounds_app_r _ _ _ &.

Lemma get_inbounds_app {A} (n : nat) {xs ys : list A} (p : inbounds xs n) : get p = get (inbounds_app ys p).
Proof.
  revert n p; induction xs; cbn; destruct n; cbn; contradiction + auto.
Defined.

Lemma get_inbounds_app_r {A} (n : nat) {xs ys : list A} (p : inbounds ys n) : get p = get (inbounds_app_r xs p).
Proof.
  revert n p; induction xs; cbn; destruct n; cbn; contradiction + auto.
Defined.

Notation rw F H x := (eq_rect _ F x _ H).

Definition cons_inbounds {A} {x : A} {xs : list A} (nn : {n | inbounds xs n})
  : {n | inbounds (x :: xs) n} := exist _ (S (proj1_sig nn)) (proj2_sig nn).


Lemma weave_last {A} (f : A -> A -> A) (x0 : A) (xs : list A) (x' : A)
  : weave f x0 (xs ++ [x']) = weave f x0 xs ++ [f (last x0 xs) x'].
Proof.
  revert x0; induction xs; cbn; intros; f_equal; auto.
Defined.

Lemma app_assoc {A} : forall xs ys zs : list A, xs ++ (ys ++ zs) = (xs ++ ys) ++ zs.
Proof.
  fix SELF 1; intros [].
  - reflexivity.
  - intros; cbn; f_equal; auto.
Defined.
