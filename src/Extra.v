From Coq Require Import List NArith.
Import ListNotations.

From IOGames Require Import
  Config Util Delay Game Core Clone Elementary Mux Fix Exec.

#[local] Open Scope game_scope.

Inductive plist : Type :=
| Nil
| Cons (g : Game) (_ : plist)
| Unknown
.

Fixpoint plist_move (p : plist) : Type :=
  match p with
  | Nil => False
  | Cons g p => move g + plist_move p
  | Unknown => unit
  end.

Fixpoint plist_comove (p : plist) : plist_move p -> Type :=
  match p with
  | Nil => fun m => match m with end
  | Cons g p => fun m =>
    match m with
    | inl m => move (play _ m)
    | inr m => plist_comove p m
    end
  | Unknown => fun _ => bool
  end.

Fixpoint plist_play (g0 : Game) (p : plist) : forall (m : plist_move p), plist_comove p m -> plist :=
  match p with
  | Nil => fun m => match m with end
  | Cons g p => fun m =>
    match m with
    | inl m => fun cm => Cons (play _ cm) p
    | inr m => fun cm => Cons g (plist_play g0 p m cm)
    end
  | Unknown => fun _ cm => if cm then Nil else Cons g0 Unknown
  end.

CoFixpoint List_ (g0 : Game) (p : plist) : Game :=
  {| move := plist_move p
  ;  play := fun m =>
     {| move := plist_comove p m
     ;  play := fun cm => List_ g0 (plist_play g0 p m cm) |}
  |}.

Definition List (g : Game) : Game := List_ g Unknown.

CoFixpoint cons_ {g0 g p} : Simulation (g * List_ g0 p) (List_ g0 (Cons g p)) := fun m =>
  match m with
  | (inl _ | inr _) as m => m ?? fun cm => cm ?? fun m' => cons_
  end%sim.

Arguments cons_ : clear implicits.
Arguments cons_ {_ _ _}.

CoFixpoint uncons_ {g0 g p} : Simulation (List_ g0 (Cons g p)) (g * List_ g0 p) := fun m =>
  match m with
  | (inl _ | inr _) as m => m ?? fun cm => cm ?? uncons_
  end%sim.

Definition foldList {g} : Simulation (Zero + (g * List g)) (List g) := fun _ =>
  (tt ?? fun cm =>
   cm ?? match cm with
         | true => fun m' => match m' with end
         | false => cons_
         end)%sim.

Arguments foldList : clear implicits.
Arguments foldList {_}.

Definition unfoldList {g} : Simulation (List g) (Zero + (g * List g)) := fun _ =>
  (tt ?? fun cm =>
   cm ?? match cm with
         | true => fun m' => match m' with end
         | false => uncons_
         end)%sim.

Arguments unfoldList : clear implicits.
Arguments unfoldList {_}.

Module Import CloneList.

Definition cloneFoldList {g} : Strategy (Clone (Zero + g * List g) *> Clone (List g)) :=
  dimapClone foldList unfoldList.

Definition cloneListBody {g}
  : Strategy (Clone g *> Clone (List g) *> Clone (List g)) :=
  applySim curry (cloneIO >>> pairwith1 cloneZero >>> cloneSum >>> cloneFoldList).

End CloneList.

Definition cloneList {g} : Strategy (Mux (Clone g) *> (Clone (List g))) :=
  Ocompose (apply apMux (pureStrategy cloneListBody)) fix_.

Arguments zero _ &.

Definition nilS {g} : TotalStrategy (List g) :=
  {| totalplay := fun _ => (true ?? {| totalplay := fun m => match m in False with end |}) |}.

Definition consS {g} : TotalStrategy (g * List g *> List g) :=
  {| totalplay := fun _ => (inr false ?? cons_) |}.

Fixpoint take {g} (n : nat) : Strategy (List g *> List g) :=
  match n with
  | O => applySim const nilS
  | S n => unfoldList >>> apply map_sum (pairIO copycatO (applySim bimapIO (pairIO copycatO (take n)))) >>> foldList
  end.

Definition Endo g := (g *> g).

Definition foldrBody {g h} : Strategy ((g *> h *> h) *> Endo (List g *> h *> h)) :=
  applySim curry (applySim curry (swap >>> applySim uncurry (unfoldList >>> apply case_sum (pairIO
    (applySim const (applySim const copycatO))
    (applySim curry (transpose22 >>> applySim bimapIO (pairIO eval eval) >>> applySim uncurry mapO)))))).

Definition foldr {g h} : Strategy (Mux (g *> h *> h) *> List g *> h *> h) :=
  apply apMux (pureStrategy foldrBody) >>> fix_.

Definition iterListBody {g} : Strategy ((g *> One unit) *> (g *> One unit *> One unit)) :=
  apply mapO (applySim curry seq).

Definition iterList {g} : Strategy (Mux (g *> One unit) *> List g *> One unit) :=
  apply apMux (pureStrategy iterListBody) >>> foldr >>>
  apply mapO (apply (applySim curry eval) (one tt)).

Definition write {A} : TotalStrategy (CoStream A *> One A *> One unit) :=
  {| totalplay := fun _ => inr (inl tt) ??
  {| totalplay := fun x => inl x ??
  {| totalplay := fun _ => inr (inr tt) ?? zero_ |} |} |}.

Definition writeList {A} : Strategy (CoStream A *> List (One A) *> One unit) :=
  apply mux (pureStrategy cloneCoStream) >>> apply apMux (pureStrategy write) >>>
  iterList.

Definition output {A} (x : A) : TotalStrategy (CoStream A *> One unit) :=
  {| totalplay := fun _ => inl x ??
  {| totalplay := fun _ => inr tt ?? zero_ |} |}%game.

Definition evaluate {A} (x : A) : Strategy (CoStream A *> One A) :=
  output x >>> pairwith2 (one x) >>> seq.

Fixpoint fromList {A} (xs : list A) : Strategy (List (One A)) :=
  match xs with
  | nil => nilS
  | cons x xs => apply consS (pairIO (one x) (fromList xs))
  end.

Fixpoint fromListLogged {A} (xs : list A) : Strategy (CoStream A *> List (One A)) :=
  match xs with
  | nil => applySim const nilS
  | cons x xs => cloneCoStream >>> applySim bimapIO (pairIO (evaluate x) (fromListLogged xs)) >>> consS
  end.

(*
let evaluate x = (output x; x)

let example1 () =
  let x = evaluate 0 in
  let y = evaluate 1 in
  [x, y]
*)
Definition example1 : Strategy (CoStream nat *> One (list nat)) :=
  cloneCoStream >>> applySim bimapIO (pairIO (evaluate 0) (evaluate 1)) >>> seqWith (fun x y => [x; y]).

(*
let example2 () =
  let x = evaluate 0 in
  [x, x]
*)
Definition example2 : Strategy (CoStream nat *> One (list nat)) :=
  evaluate 0 >>> cloneOne >>> seqWith (fun x y => [x; y]).

(*
let example3 () =
  let xs = map evaluate [1;2;3] in
  writeList xs
*)
Definition example3 : Strategy (CoStream nat *> One unit) :=
  cloneCoStream >>> applySim bimapIO (pairIO copycatO (fromListLogged [1;2;3])) >>>
  applySim uncurry writeList.

(*
let example4 () =
  let xs = map evaluate [1;2;3;4] in
  let ys = take 2 xs in
  let zs = take 3 xs in
  writeList ys; writeList zs
*)
Definition example4 : Strategy (CoStream nat *> One unit) :=
  cloneCoStream >>> applySim bimapIO (pairIO copycatO
    ( fromListLogged [1;2;3;4] >>>
      apply cloneList (pureStrategy cloneOne) >>>
      applySim bimapIO (pairIO (take 2) (take 3))
    )) >>>
  applySim uncurry
    ( cloneCoStream >>>
      applySim curry
        ( transpose22 >>>
          applySim bimapIO (pairIO (applySim uncurry writeList) (applySim uncurry writeList)) >>>
          seq)).

CoFixpoint runStrategy_ {A B} (s : CoStrategy (CoStream A * Half B Zero)) (xs : list A)
  : delay (list A * B) :=
  awaiting s (fun s =>
    match next s as n return Strategy (play (CoStream A * _) n) -> _ with
    | inl x => fun s => Step (runStrategy_ (coplay s tt) (x :: xs))
    | inr y => fun _ => Stop (rev' xs, y)
    end (after s)).

Definition runStrategy {A B} (s : Strategy (CoStream A *> One B)) : delay (list A * B) :=
  runStrategy_ (coplay s tt) [].

Arguments runStrategy_ {A B s} xs. (* Hide Strategy from display *)

Compute (force (runStrategy example1) 300). (* prints 0 then 1 *)
Compute (force (runStrategy example2) 300). (* prints 0 only once *)
Compute (force (runStrategy example3) 5000).
Compute (force (runStrategy example4) 10000).
