From IOGames Require Import
  Config Util Delay Game Core.

#[local] Open Scope game_scope.

CoInductive Bisimulation (g1 g2 : Game) : Type := MkBisimulation
  { eqmove : move g1 -> move g2 -> Type
  ; eqplay : forall m1 m2, eqmove m1 m2 -> Bisimulation (play g1 m1) (play g2 m2) }.

Arguments MkBisimulation _ _ &.

CoFixpoint bisim_refl {g : Game} : Bisimulation g g :=
  {| eqmove := eq
  ;  eqplay := fun _ _ p =>
       match p with
       | eq_refl => bisim_refl
       end |}.

Record prod_rel {A1 B1 A2 B2} (r1 : A1 -> B1 -> Type) (r2 : A2 -> B2 -> Type)
    (x : A1 * A2) (y : B1 * B2) : Type :=
  { fst_rel : r1 (fst x) (fst y)
  ; snd_rel : r2 (snd x) (snd y)
  }.

Inductive sum_rel {A1 B1 A2 B2} (r1 : A1 -> B1 -> Type) (r2 : A2 -> B2 -> Type)
  : A1 + A2 -> B1 + B2 -> Type :=
| inl_rel {x y} : r1 x y -> sum_rel r1 r2 (inl x) (inl y)
| inr_rel {x y} : r2 x y -> sum_rel r1 r2 (inr x) (inr y)
.

CoFixpoint bisimIO {g1 h1 g2 h2} (b1 : Bisimulation g1 h1) (b2 : Bisimulation g2 h2)
  : Bisimulation (g1 * g2) (h1 * h2) :=
  {| eqmove := sum_rel (eqmove b1) (eqmove b2)
  ;  eqplay := sum_rel_rect (fun m1 m2 _ => Bisimulation (play (g1 * g2) m1) (play (h1 * h2) m2))
       (fun _ _ p => bisimI (eqplay b1 _ _ p) b2)
       (fun _ _ p => bisimO b1 (eqplay b2 _ _ p)) |}
with bisimI {g1 h1 g2 h2} (b1 : Bisimulation g1 h1) (b2 : Bisimulation g2 h2)
  : Bisimulation (g1 <* g2) (h1 <* h2) :=
  {| eqmove := eqmove b1
  ;  eqplay := fun _ _ p => bisimIO (eqplay b1 _ _ p) b2 |}
with bisimO {g1 h1 g2 h2} (b1 : Bisimulation g1 h1) (b2 : Bisimulation g2 h2)
  : Bisimulation (g1 *> g2) (h1 *> h2) :=
  {| eqmove := eqmove b2
  ;  eqplay := fun _ _ p => bisimIO b1 (eqplay b2 _ _ p) |}.

Record Action_rel {g1 g2 X1 X2} (rg : Bisimulation g1 g2)
    (rX : forall g1 g2, Bisimulation g1 g2 -> X1 g1 -> X2 g2 -> Type)
    (s1 : Action g1 X1) (s2 : Action g2 X2)
  : Type := MkAction_rel
  { next_rel : eqmove rg (next s1) (next s2)
  ; after_rel : rX _ _ (eqplay rg _ _ next_rel) (after s1) (after s2)
  }.

Arguments MkAction_rel _ _ _ _ _ _ _ _ &.

CoInductive Strategy_rel {g1 g2} (b : Bisimulation g1 g2) (s1 : Strategy g1) (s2 : Strategy g2)
  : Type := MkStrategy_rel
  { eqcoplay : forall m1 m2 (p : eqmove b m1 m2),
      delay_rel (Action_rel (eqplay b m1 m2 p) (fun _ _ => Strategy_rel)) (coplay s1 m1) (coplay s2 m2)
  }.

Arguments MkStrategy_rel _ _ _ _ _ &.

CoFixpoint copycat_rel {g1 g2} (b : Bisimulation g1 g2)
  : Strategy_rel (bisimO b b) copycatO copycatO :=
  {| eqcoplay := fun _ _ p => stop_rel (MkAction_rel (inl_rel p) (copycatI_rel _)) |}
with copycatI_rel {g1 g2} (b : Bisimulation g1 g2)
  : Strategy_rel (bisimI b b) copycatI copycatI :=
  {| eqcoplay := fun _ _ p => stop_rel (MkAction_rel (inr_rel p) (copycat_rel _)) |}.

(** [apply copycatO : Strategy g -> Strategy g] is the identity *)

Section ApplyCopyCatId.

Context (apply : forall g1 g2 (s2 : Strategy (g1 *> g2)) (s1 : Strategy g1), Strategy g2).

Context (apply_copycat_id_ : forall {g} (s : Strategy g), Strategy_rel bisim_refl (apply copycatO s) s).

Lemma coterminating_coapply_copycat {g} (m : move g) (s : Strategy g)
  : terminating (coapplyFun apply (coplay copycatO m) s) <-> terminating (coplay s m).
Proof.
  rewrite coterminating_Step at 1; cbn.
  rewrite coterminating_eta at 1; cbn delta iota zeta; rewrite <- coterminating_eta.
  apply coterminating_awaiting; intros.
  apply terminating_Step, coterminating_Step; cbn; constructor.
Qed.

Lemma terminating_coapplyFun_copycatI {g}
  : forall p : Action g Strategy, let '(m ?? s) := p in
      terminating (Later (Step (coapplyFun apply (coplay copycatI m) s))).
Proof.
  intros p; cbn.
  apply terminating_Step.
  apply terminating_eta; cbn. apply terminating_Stop.
Defined.

Lemma value_coapply_copycat {g} (m : move g) (s : Strategy g)
  : forall a, coplay s m !! a ->
      ( coapplyFun apply (coplay copycatO m) s
      = stop (let '(m ?? s) := a in m ?? apply copycatO s))%delay.
Proof.
  intros a Va. rewrite eq_eta; cbn; rewrite eq_Step.
  rewrite eq_eta; cbn - [awaiting]; rewrite <- eq_eta.
  rewrite eq_awaiting with (a := a) by assumption.
  rewrite eq_Step.
  rewrite eq_eta; cbn.
  reflexivity.
Qed.

Lemma eq_value_coapply_copycat {g} (m : move g) (s : Strategy g)
  : forall a1 a2,
      coapplyFun apply (coplay copycatO m) s !! a1 ->
      coplay s m !! a2 ->
    Action_rel bisim_refl (fun _ _ => Strategy_rel) a1 a2.
Proof.
  intros a1 a2 V1 V2. erewrite value_coapply_copycat in V1 by eassumption.
  cbn in V1; apply value_stop_inv in V1. rewrite <- V1.
  cbn; refine (MkAction_rel (rg := bisim_refl) eq_refl _).
  cbn; apply apply_copycat_id_.
Defined.

Lemma coapply_copycat_id__ {g} (m : move g) (s : Strategy g)
  : delay_rel (Action_rel bisim_refl (fun _ _ => Strategy_rel)) (coapplyFun apply (coplay copycatO m) s) (coplay s m).
Proof.
  constructor.
  - apply coterminating_coapply_copycat.
  - apply eq_value_coapply_copycat.
Defined.

End ApplyCopyCatId.

CoFixpoint apply_copycat_id {g} (s : Strategy g) : Strategy_rel bisim_refl (apply copycatO s) s :=
  {| eqcoplay := fun _ _ p =>
       match p with
       | eq_refl => coapply_copycat_id__ _ (@apply_copycat_id) _ s
       end |}.
