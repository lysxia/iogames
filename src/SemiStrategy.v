From IOGames Require Import
  Config Util Delay Game Core.

#[local] Open Scope game_scope.

CoInductive SemiStrategy (g g_ : Game) : Type := MkSemiStrategy
  { semiplay : forall (m_ : move g_),
      Action (play g_ m_) (fun g_ => SemiStrategy g g_)
      + Action g (fun g => forall (m : move g),
        Action (play g_ m_) (fun g_ => SemiStrategy (play g m) g_)) }.

Arguments MkSemiStrategy _ _ &.

Definition Stratagem (g : Game) : Type := forall g_, SemiStrategy g g_ -> TotalStrategy g_.

CoFixpoint idSemiStrategy {g} : SemiStrategy g g :=
  {| semiplay := fun m => inr (m ?? fun cm => cm ?? idSemiStrategy) |}.

CoFixpoint catSemiStrategy {g1 g2 g3} (s1 : SemiStrategy g1 g2) (s2 : SemiStrategy g2 g3)
  : SemiStrategy g1 g3 :=
  {| semiplay := fun m3 =>
       match semiplay s2 m3 with
       | inl (cm3 ?? s2) => inl (cm3 ?? catSemiStrategy s1 s2)
       | inr (m2 ?? s2) =>
         match semiplay s1 m2 with
         | inl (cm2 ?? s1) => let '(cm3 ?? s2) := s2 cm2 in
           inl (cm3 ?? catSemiStrategy s1 s2)
         | inr (cm1 ?? s1) =>
           inr (cm1 ?? fun cm1 =>
             let '(cm2 ?? s1) := s1 cm1 in
             let '(cm3 ?? s2) := s2 cm2 in
             cm3 ?? catSemiStrategy s1 s2)
         end
       end |}.

Definition mapStratagem {g1 g2} (s : SemiStrategy g1 g2) (z : Stratagem g1) : Stratagem g2 :=
  fun g_ s2 => z g_ (catSemiStrategy s s2).

(* This is currently broken *)
Definition refineStratagem {g}
    (eta : forall (P : move g -> Type), (forall m, P m) -> (forall m, P m))
    (handle : forall (m : move g), Action (play g m) Stratagem)
  : Stratagem g :=
  cofix _refineStratagem g_ s :=
  {| totalplay := fun m_ =>
       match semiplay s m_ with
       | inl cm_ => {| next := next cm_ ; after := _refineStratagem _ (after cm_) |}
       | inr m =>
         (fun k => eta (fun m => (forall cm_ : move (play g m), Action _ (SemiStrategy (play _ cm_))) -> _) k (next m) (after m)) (fun m am =>
         let z := handle m in
         let cm_ := am (next z) in
         {| next := next cm_ ; after := after z _ (after cm_) |})
       end |}.

Arguments refineStratagem _ &.

CoFixpoint mapSimulationSemi {g1 g2} (s : Simulation g1 g2) : SemiStrategy g1 g2 :=
  {| semiplay := fun m2 => let '(m1 ?? s)%sim := s m2 in inr @@
       m1 ?? fun cm1 => let '(cm2 ?? s)%sim := s cm1 in
       cm2 ?? mapSimulationSemi s |}.

Coercion mapSimulationSemi : Simulation >-> SemiStrategy.

CoFixpoint liftSSO {g1 g2 g2'} (s : SemiStrategy g2 g2') : SemiStrategy (g1 *> g2) (g1 *> g2') :=
  {| semiplay := fun m2' =>
       match semiplay s m2' with
       | inl (cm2 ?? s) => inl (inr cm2 ?? liftSSO s)
       | inr (m2 ?? s) => inr (m2 ?? fun cm =>
           match cm with
           | inl m1 => inl m1 ?? liftSSO_ s
           | inr cm2 => let '(cm2' ?? s) := s cm2 in inr cm2' ?? liftSSO s
           end)
       end |}
with liftSSO_ {g1 g2 g2'} (s : forall (m : move g2), Action g2' (SemiStrategy (play g2 m)))
  : SemiStrategy (g1 <* g2) (g1 <* g2') :=
  {| semiplay := fun m1 => inr (m1 ?? fun cm =>
       match cm with
       | inl cm1 => inl cm1 ?? liftSSO_ s
       | inr m2 => let '(m2' ?? s) := s m2 in inr m2' ?? liftSSO s
       end)
  |}.
