From IOGames Require Import Config Util Game Core.

#[local] Open Scope game_scope.

Definition Clone (g : Game) : Game := (g *> g * g).

Class Cloneable (g : Game) : Type :=
  clone : Strategy (Clone g).

Definition dimapClone {g1 g2} (s1 : Strategy (g1 *> g2)) (s2 : Strategy (g2 *> g1))
  : Strategy (Clone g1 *> Clone g2) :=
  apply (dimapO_ >>> mirrorIO) (pairIO s2 (apply (bimapIO >>> mirrorOI) (pairIO s1 s1))).

Definition cloneIO {g1 g2} : Simulation (Clone g1 * Clone g2) (Clone (g1 * g2)) :=
  (bimapIO >>> dimapSimO idSimulation transpose22)%sim.
