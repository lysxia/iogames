(** * IOGames *)

(* Can be thought of as an indexed generalization of interaction trees.

   Effect signatures  (g : Game)                     <---   (E : Type -> Type)
   Computations       (s : Strategy g)             <---   (t : itree E _)
   Handlers           (s : Strategy (g1 *> g2))    <---   (h : E2 ~> itree E1)

   The generalization allows us to represent stateful protocols, which
   specify the order in which events may be triggered.

   The category of CoStrategies (defined by Hom g1 g2 := Strategy (g1 *> g2))
   is monoidal (with tensor [*]).
   It also has an exponential-like operator [*>], although it is not a CCC
   (there is no cartesian product). It also has a fix-like operator.

   The original motivation for this was to reason about lazy programs in Coq:

   - TODO It enables amortized complexity analysis of lazy data structures.
     (we still have trouble doing that using ICFP'21 (Li, Xia, Weirich))

   - TODO We generalize QuickChick's theory of generators for functions and
     coinductive types. (QuickChick has a completeness theorem only for "first-order"
     functions, whose domains are made of finite objects (aka. discrete domains)).
*)

(** Interpretation of lazy functions as games:

   Intuitively, a lazy function (g1 -> g2) denotes a costrategy on a game (g1 *> g2)
   with the following "rules":
   the environment first "asks a question" to the function on its output (g2),
   the function reacts by asking (zero or more) questions on the input (g1),
   the environment should answer those, until the function answers the
   initial question on the output (g2), prompting the environment for another
   question on the output.
   In brief, a function [g1 -> g2] is represented by a function from output
   demands to input demands.
 *)

From IOGames Require Import Config Util Delay.

CoInductive Game : Type :=
  { move : Type
  ; play : move -> Game
  }.

Declare Scope game_scope.
Delimit Scope game_scope with game.
Bind Scope game_scope with Game.

Record Action (g : Game) (X : Game -> Type) : Type := MkAction
  { next : move g
  ; after : X (play g next)
  }.

Infix "??" := MkAction (at level 90) : game_scope.

(* A [Strategy] is a "strategy" for the second player,
   i.e., it answers the first player's moves with countermoves.

   It may diverge in the [delay] monad (which is necessary to define composition). *)
CoInductive Strategy (g : Game) : Type := MkStrategy
  { coplay : forall (m : move g), delay (Action (play g m) Strategy) }.

Notation Strategy_ g := (forall (m : move g), delay (Action (play g m) Strategy)).

(* A [CoStrategy] is one played by the first player. *)
Definition CoStrategy (g : Game) : Type := delay (Action g Strategy).

Arguments MkAction g X &.
Arguments MkStrategy g &.

(**)

(** A [TotalStrategy] always has an answer to any move.
    The composition operator [Ocompose] cannot be defined for it, but many
    other core combinators are actually total strategies. *)
CoInductive TotalStrategy (g : Game) : Type := MkTotalStrategy
  { totalplay : forall (m : move g), Action (play g m) TotalStrategy }.

Arguments MkTotalStrategy g &.

(**)

(* We could define [Simulation] itself as a record, but at the cost of an extra [sigT],
   which is annotying. Luckily, the types align just right for [cofix] to still
   look identical. *)
CoInductive Simulation_ (g1 g2 : Game) : Type := MkSimulation_
  { stick : move g1
  ; stock : forall m : move (play g1 stick), Simulation_ g2 (play _ m)
  }.

Definition Simulation (g1 g2 : Game) : Type :=
  forall m : move g2, Simulation_ g1 (play g2 m).

Arguments MkSimulation_ _ _ &.

Declare Scope sim_scope.
Delimit Scope sim_scope with sim.
Bind Scope sim_scope with Simulation.
Bind Scope sim_scope with Simulation_.

Infix "??" := MkSimulation_ : sim_scope.
