(** * Exponential modality *)

(** [Mux g = !g]  *)

From Coq Require Import
  List.
Import ListNotations.

From IOGames Require Import
  Config Util Delay Game Core Clone Elementary SemiStrategy.

#[local] Open Scope game_scope.

Inductive move_Mux (gs : list Game) : Type :=
| Spawn
| Child (n : nat) (p : inbounds gs n) (_ : move (get p))
.

Arguments Child _ &.

Definition CoMux__ (Mux_ : list Game -> Game) (gbefore gafter : list Game) (g : Game)
  : Game :=
  {| move := move g
  ;  play := fun cm => Mux_ (gbefore ++ play g cm :: gafter)
  |}.

CoFixpoint Mux_ (g : Game) (gs : list Game) : Game :=
  {| move := move_Mux gs
  ;  play := fun m =>
       match m with
       | Spawn => Ack (Mux_ g (g :: gs))
       | Child n p m => CoMux__ (Mux_ g) (before p) (afterl p) (play (get p) m)
       end
  |}.

Definition CoMux_ g := CoMux__ (Mux_ g).

Definition Mux g : Game := Mux_ g nil.

Module Private_cloneMux.

Inductive dir := Ldir | Rdir.

Definition interleaving (A : Type) : Type := list (dir * A).

Definition tangle {A} : interleaving A -> list A := map snd.

Fixpoint untangle {A} (d : dir) (xs : interleaving A) : list A :=
  match xs, d with
  | [], _ => []
  | (Ldir, x) :: xs, Ldir | (Rdir, x) :: xs, Rdir => x :: untangle d xs
  | _ :: xs, _ => untangle d xs
  end.

Definition codomSS (g : Game) (gs : interleaving Game) : Type :=
  Simulation
      (Mux_ g (tangle gs))
      (Mux_ g (untangle Ldir gs) * Mux_ g (untangle Rdir gs)).

Section Private_cloneMux.
Context (cloneMuxS0 : forall g gs, codomSS g gs).

#[local] Arguments existT _ _ &.

Fixpoint tangle_elem {A} d {xs : interleaving A} {n} : inbounds (untangle d xs) n -> {m | inbounds (tangle xs) m } :=
  match xs, d with
  | [], _ => fun v => match v in False with end
  | (Ldir as xd, _) :: xs, Ldir as d | (Rdir as xd, _) :: xs, Rdir as d =>
    match n with
    | O => fun _ => exist _ O I
    | S n => fun p => cons_inbounds (tangle_elem d (xs := xs) (n := n) p)
    end
  | _ :: xs, d => fun p => cons_inbounds (tangle_elem d (xs := xs) p)
  end.

Fixpoint tangle_get {A} {P : A -> Type} d {xs : interleaving A} {n}
  : forall p : inbounds (untangle d xs) n, P (get p) -> P (get (proj2_sig (tangle_elem d p))) :=
  match xs, d with
  | [], _ => fun v => match v in False with end
  | (Ldir, _) :: xs, Ldir as d | (Rdir, _) :: xs, Rdir as d =>
      match n with
      | O => fun _ m => m
      | S n => fun p => tangle_get d (xs := xs) p
      end
  | _ :: xs, d => fun p => tangle_get d (xs := xs) p
  end.

Fixpoint reinterleave {A} d {xs : interleaving A} {n}
  : inbounds (untangle d xs) n -> A -> interleaving A :=
  match xs, d return inbounds (untangle d xs) n -> _ with
  | [], _ => fun v => match v in False with end
  | (Ldir, _) as x :: xs, Ldir as d | (Rdir, _) as x :: xs, Rdir as d =>
      match n with
      | O => fun p y => (d, y) :: xs
      | S _ => fun p y => x :: reinterleave d (xs := xs) p y
      end
  | x :: xs, d => fun p y => x :: reinterleave d (xs := xs) p y
  end.

Fixpoint replay d {xs : interleaving Game} {n}
  : forall (p : inbounds (untangle d xs) n) (m : move (get p)),
      move (play _ (tangle_get d p m)) -> move (play _ m) :=
  match xs, d with
  | [], _ => fun v => match v in False with end
  | (Ldir, _) :: xs, Ldir as d | (Rdir, _) :: xs, Rdir as d =>
      match n with
      | O => fun _ _ m => m
      | S _ => fun p => replay d (xs := xs) p
      end
  | _ :: xs, d => fun p => replay d (xs := xs) p
  end.

Lemma f3 {A1 A2 A3 B} (P : A1 -> A2 -> A3 -> B) (x1 y1 : A1) (x2 y2 : A2) (x3 y3 : A3)
  : (forall Q : A1 -> A2 -> A3 -> B, Q x1 x2 x3 = Q y1 y2 y3) ->
    P x1 x2 x3 = P y1 y2 y3.
Proof. exact (fun H => H P). Defined.

Definition rwL (d := Ldir) {g gs} {n} {p : inbounds (untangle d gs) n} {m : move (get p)}
    {cm : move (play _ (tangle_get d p m))}
    (gs' := reinterleave d p (play _ cm))
  : ( Mux_ g (tangle gs')
    , ( Mux_ g (untangle Ldir gs')
      * Mux_ g (untangle Rdir gs') )%game )
  = ( Mux_ g (before (proj2_sig (tangle_elem d p)) ++ play _ cm :: afterl (proj2_sig (tangle_elem d p)))
    , ( Mux_ g (before p ++ play _ (replay d p m cm) :: afterl p)
      * Mux_ g (untangle Rdir gs) )%game ).
Proof.
  apply f3 with (P := fun x1 x2 x3 => (Mux_ g x3, Mux_ g x1 * Mux_ g x2)%game).
  revert n p m cm gs'; induction gs as [ | [ [] ? ] gs IH ]; intros.
  - destruct p.
  - destruct n as [ | ]; cbn; [ reflexivity | ].
    apply IH with (Q := fun x1 x2 x3 => Q (_ x1) x2 (_ x3)).
  - apply IH with (Q := fun x1 x2 x3 => Q x1 (_ x2) (_ x3)).
Defined.

Definition rwR (d := Rdir) {g gs} {n} {p : inbounds (untangle d gs) n} {m : move (get p)}
    {cm : move (play _ (tangle_get d p m))}
    (gs' := reinterleave d p (play _ cm))
  : ( Mux_ g (tangle gs')
    , ( Mux_ g (untangle Ldir gs')
      * Mux_ g (untangle Rdir gs') )%game )
  = ( Mux_ g (before (proj2_sig (tangle_elem d p)) ++ play _ cm :: afterl (proj2_sig (tangle_elem d p)))
    , ( Mux_ g (untangle Ldir gs)
      * Mux_ g (before p ++ play _ (replay d p m cm) :: afterl p) )%game ).
Proof.
  apply f3 with (P := fun x1 x2 x3 => (Mux_ g x3, Mux_ g x1 * Mux_ g x2)%game).
  revert n p m cm gs'; induction gs as [ | [ [] ? ] gs IH ]; intros; cbn.
  - destruct p.
  - apply IH with (Q := fun x1 x2 x3 => Q (_ x1) x2 (_ x3)).
  - destruct n as [ | n ]; cbn; [ reflexivity | ].
    apply IH with (Q := fun x1 x2 x3 => Q x1 (_ x2) (_ x3)).
Defined.

Definition scoerce {A B} (e : A = B) (x : Simulation (fst A) (snd A)) : Simulation (fst B) (snd B) :=
  eq_rect _ (fun T => Simulation (fst T) (snd T)) x _ e.

Definition cloneMuxS__ g gs
  : Simulation (Mux_ g (tangle gs))
               (Mux_ g (untangle Ldir gs) * Mux_ g (untangle Rdir gs)) := fun m =>
  match m return Simulation_ (Mux_ g (tangle gs)) (play (Mux_ g (untangle Ldir gs) * Mux_ g (untangle Rdir gs)) m) with
  | inl Spawn => Spawn ?? fun _ => tt ?? cloneMuxS0 g ((Ldir, g) :: gs)
  | inr Spawn => Spawn ?? fun _ => tt ?? cloneMuxS0 g ((Rdir, g) :: gs)
  | inl (Child n p m) =>
    Child _ _ (tangle_get Ldir p m) ?? fun cm =>
    replay Ldir p m cm ?? scoerce rwL (cloneMuxS0 g _)
  | inr (Child n p m) =>
    Child _ _ (tangle_get Rdir p m) ?? fun cm =>
    replay Rdir p m cm ?? scoerce rwR (cloneMuxS0 g _)
  end%sim.

End Private_cloneMux.

CoFixpoint cloneMuxS_ : forall g gs, codomSS g gs := cloneMuxS__ cloneMuxS_.

End Private_cloneMux.

Definition cloneMuxS {g} : Simulation (Mux g) (Mux g * Mux g) :=
  Private_cloneMux.cloneMuxS_ g [].

Definition cloneMux {g} : Strategy (Clone (Mux g)) := cloneMuxS.

Module Private_mux.

Inductive Mux1S_ gs g' n (p : inbounds (gs ++ [g']) n) (m : move (get p)) : Type :=
| Mux1SInit (p' : inbounds gs n) (m' : move (get p'))
    (counter : forall cm' : move (play _ m'),
       Action (play _ m) (fun g_ =>
         ((before p' ++ play _ cm' :: afterl p') ++ [g']) = (before p ++ g_ :: afterl p)))
| Mux1SLast (m' : move g')
    (counter : forall cm' : move (play _ m'),
      Action (play _ m) (fun g_ =>
        (gs ++ [play _ cm']) = (before p ++ g_ :: afterl p)))
.

Arguments Mux1SLast _ _ _ _ _ _ &.
Arguments Mux1SInit _ _ _ _ _ _ _ &.

Fixpoint mkMux1S_ {gs g' n} {struct gs} : forall p m, Mux1S_ gs g' n p m :=
  match gs with
  | [] =>
    match n with
    | O => fun p m => Mux1SLast m (fun cm => cm ?? eq_refl)
    | S _ => fun p _ => match p in False with end
    end
  | g :: gs =>
    match n with
    | O => fun p m => Mux1SInit (gs := g :: gs) (n := O) p m (fun cm => cm ?? eq_refl)
    | S n => fun p m =>
        match mkMux1S_ (gs := gs) (n := n) (p := p) (m := m) with
        | Mux1SInit p' m' counter => Mux1SInit (gs := g :: gs) (n := S _) p' m' (fun cm' =>
            let '(cm ?? H) := counter cm' in
            cm ?? f_equal (cons _) H)
        | Mux1SLast m' counter => Mux1SLast m' (fun cm' =>
            let '(cm ?? H) := counter cm' in
            cm ?? f_equal (cons _) H)
        end
    end
  end.

CoFixpoint mux1S_ {g gs g'} : Simulation (Mux_ g gs * g') (Mux_ g (gs ++ [g'])) := fun m =>
  match m with
  | Spawn => inl Spawn ?? fun _ => tt ?? mux1S_ (gs := (g :: gs))
  | Child n p m =>
    match mkMux1S_ (p := p) (m := m) with
    | Mux1SInit p' m' counter =>
      inl (Child _ p' m') ?? fun cm' => let '(cm ?? H)%game := counter cm' in
      cm ?? rw (fun gs_ => Simulation _ (Mux_ g gs_)) H mux1S_
    | Mux1SLast m' counter =>
      inr m' ?? fun cm' => let '(cm ?? H)%game := counter cm' in
      cm ?? rw (fun gs_ => Simulation _ (Mux_ g gs_)) H mux1S_
    end
  end%sim.

Definition mux1S {g g'} : Simulation (Mux g * g') (Mux_ g [g']) := mux1S_ (gs := []).

Inductive Mux1S'1 {gs} g' {n} (p : inbounds gs n) (m : move (get p)) : Type :=
| MkMux1S'1 (p' : inbounds (gs ++ [g']) n) (m' : move (get p'))
    (counter : forall cm' : move (play _ m'),
       Action (play _ m) (fun g =>
         (before p ++ g :: afterl p) ++ [g'] = before p' ++ play _ cm' :: afterl p'))
.

Arguments MkMux1S'1 _ _ _ _ &.

Inductive Mux1S'2 gs {g'} (m : move g') : Type :=
| MkMux1S'2 n' (p' : inbounds (gs ++ [g']) n') (m' : move (get p'))
    (counter : forall cm' : move (play _ m'),
       Action (play _ m) (fun g => gs ++ [g] = before p' ++ play _ cm' :: afterl p'))
.

Arguments MkMux1S'2 _ _ _ &.

Fixpoint mkMux1S'1 {gs g' n} : forall (p : inbounds gs n) (m : move (get p)), Mux1S'1 g' p m :=
  match gs with
  | [] => fun p => match p in False with end
  | g :: gs =>
    match n with
    | O => fun p m => MkMux1S'1 (gs := g :: gs) (n := O) p m (fun cm' => cm' ?? eq_refl)
    | S _ => fun p m =>
      match mkMux1S'1 (gs := gs) (p := p) (m := m) with
      | MkMux1S'1 p' m' counter => MkMux1S'1 p' m' (fun cm' =>
        let '(cm ?? H) := counter cm' in
        cm ?? f_equal (cons _) H)
      end
    end
  end.

Fixpoint mkMux1S'2 {gs g'} (m : move g') : Mux1S'2 gs m :=
  match gs with
  | [] => MkMux1S'2 (gs := []) O I m (fun cm' => cm' ?? eq_refl)
  | g :: gs =>
    match mkMux1S'2 (gs := gs) with
    | MkMux1S'2 n' p' m' counter => MkMux1S'2 (S n') p' m' (fun cm' =>
      let '(cm ?? H) := counter cm' in
      cm ?? f_equal (cons _) H)
    end
  end.

CoFixpoint mux1S'_ {g gs g'} : Simulation (Mux_ g (gs ++ [g'])) (Mux_ g gs * g') := fun m =>
  match m return Simulation_ (Mux_ g (gs ++ [g'])) (play (Mux_ g gs * g') m) with
  | inl Spawn => Spawn ?? fun _ => tt ?? mux1S'_ (gs := (g :: gs))
  | inl (Child _ p m) =>
    match mkMux1S'1 (p := p) (m := m) with
    | MkMux1S'1 p' m' counter =>
      Child _ p' m' ?? fun cm' => let '(cm ?? H)%game := counter cm' in
      cm ?? rw (fun gs_ => Simulation (Mux_ g gs_) _) H mux1S'_
    end
  | inr m =>
    match mkMux1S'2 with
    | MkMux1S'2 _ p' m' counter =>
      Child _ p' m' ?? fun cm' => let '(cm ?? H)%game := counter cm' in
      cm ?? rw (fun gs_ => Simulation (Mux_ g gs_) _) H mux1S'_
    end
  end%sim.

Definition mux1S' {g g'} : Simulation (Mux_ g [g']) (Mux g * g') := mux1S'_ (gs := []).

Module AfterSpawn_11.

Inductive S : Game -> Game -> Type :=
| Init {g__ g1 g2 g3}
  : S (g3 *> g__) ((g1 *> g2 * g3) *> g1 *> (g__ * g2))
| AfterG2 {g__ g1 g2 g3}
  : S (g3 *> g__) ((g1 * (g2 <* g3)) <* (g1 * (g__ *> g2)))
| AfterG2G1 {g__ g1 g2 g3}
  : S (g3 *> g__) ((g1 <* (g2 <* g3)) *> (g1 <* (g__ *> g2)))
| AfterG3 {g__ g1 g2 g3}
  : S (g3 <* g__) ((g1 * (g2 *> g3)) <* (g1 * (g__ <* g2)))
| AfterG3G1 {g__ g1 g2 g3}
  : S (g3 <* g__) ((g1 <* (g2 *> g3)) *> (g1 <* (g__ <* g2)))
.

Section AS11.

Context (afterSpawn_11_ : forall g g_, S g g_ -> SemiStrategy g g_).

Arguments afterSpawn_11_ {g g_}.

Definition afterG3' {g__ g1 g2 g3} (cm : move (g3 * g__))
  : Action ((g1 *> g2 * g3) * (g1 * (g__ <* g2))) (SemiStrategy (play (g3 * g__) cm)) :=
  match cm with
  | inl m3 => inl (inr m3) ?? afterSpawn_11_ AfterG3
  | inr cm => inr (inr cm) ?? afterSpawn_11_ Init
  end.

Definition afterSpawn_11__ {g g_} (s : S g g_) : SemiStrategy g g_ :=
  match s with
  | Init =>
    {| semiplay := fun m_ : move (_ *> (_ *> (_ * _))) =>
         match m_ with
         | inl m__ => inr (m__ ?? afterG3')
         | inr m2 => inl (inl (inl m2) ?? afterSpawn_11_ AfterG2)
         end |}
  | AfterG2 =>
    {| semiplay := fun m =>
         match m with
         | inl m1 => inl (inr (inl m1) ?? afterSpawn_11_ AfterG2G1)
         | inr m2 => inl (inr (inr m2) ?? afterSpawn_11_ Init)
         end |}
  | AfterG2G1 =>
    {| semiplay := fun m => inl (inl m ?? afterSpawn_11_ AfterG2) |}
  | AfterG3 =>
    {| semiplay := fun m =>
         match m with
         | inl m1 => inl (inr (inl m1) ?? afterSpawn_11_ AfterG3G1)
         | inr m3 => inr (m3 ?? afterG3')
         end |}
  | AfterG3G1 =>
    {| semiplay := fun m => inl (inl m ?? afterSpawn_11_ AfterG3) |}
  end.

End AS11.

CoFixpoint afterSpawn_11_ {g g_} (s : S g g_) : SemiStrategy g g_ :=
  afterSpawn_11__ (@afterSpawn_11_) s.

End AfterSpawn_11.

Section AfterSpawn_11_.
Import AfterSpawn_11.

Definition afterSpawn_11_ {g__ g1 g2 g3}
  : SemiStrategy (g3 *> g__) ((g1 *> g2 * g3) *> g1 *> (g__ * g2)) :=
  afterSpawn_11_ Init.

End AfterSpawn_11_.

Definition afterSpawn_1_ {g_ g__ g1 g2 g3}
  : SemiStrategy (g_ *> g3 *> g__)
                 (g_ * (g1 *> g2 * g3) *> g1 *> (g__ * g2)) :=
  catSemiStrategy (liftSSO afterSpawn_11_) uncurry.

Definition afterSpawn_1 {g}
  : SemiStrategy (Mux (g *> g * g) *> g *> Mux g)
                 (Mux (g *> g * g) * (g *> g * g) *> g *> (Mux g * g)) := afterSpawn_1_.

Definition afterSpawn_2 {g}
  : SemiStrategy (Mux (g *> g * g) * (g *> g * g) *> g *> (Mux g * g))
                 (Mux_ (g *> g * g) [g *> g * g]%game *> (g *> Mux_ g [g])) :=
  dimapSimO mux1S' (dimapSimO idSimulation mux1S).

Definition afterSpawn {g}
  : SemiStrategy (Mux (g *> g * g) *> g *> Mux g)
                 (Mux_ (g *> g * g) [g *> g * g]%game *> (g *> Mux_ g [g])) :=
  catSemiStrategy afterSpawn_1 afterSpawn_2.

CoFixpoint mux_ {g} : Stratagem (Mux (g *> g * g) *> g *> Mux g).
Proof.
  intros g_ s; constructor; intros m_.
  destruct (semiplay s m_) as [ [cm_ s_] | [m s'] ].
  { exists cm_. exact (mux_ _ _ s_). }
  destruct m as [ | n p m].
  2:{ destruct p. }
  specialize (s' (inl Spawn)).
  destruct s' as [m2 s2].
  exists m2.
  revert s2; generalize (play _ m2); clear m2.
  cofix mux__; intros; constructor; intros m__.
  destruct (semiplay s2 m__) as [ [cm_ s_] | [m3 s3] ].
  { exists cm_. exact (mux__ _ s_). }
  specialize (s3 (inr (inr tt))).
  destruct s3 as [m4 s4].
  exists m4.
  cbn in s4. apply (catSemiStrategy afterSpawn) in s4.
  apply (mux_ _ _ s4).
Defined.

CoFixpoint demux_ {g0 g} : TotalStrategy (Mux_ g0 [g] *> g) :=
  totalplaying m => inl (Child 0 I m) ??
  totalplaying cm => inr cm ?? demux_.

End Private_mux.

Definition mux {g} : TotalStrategy (Mux (g *> g * g) *> g *> Mux g) :=
  Private_mux.mux_ idSemiStrategy.

Definition demux {g} : TotalStrategy (Mux g *> g) :=
  totalplaying m => inl Spawn ??
  totalplaying _ => totalplay (Private_mux.demux_ (g := g)) m.

Module Private_pureStrategy.

Notation Ss := (list {g : Game & Strategy g}).

Section Private_pureStrategy.

Context {g0} (s0 : Strategy g0).

Context (pureStrategy_ : forall (zs : Ss), Strategy (Mux_ g0 (map (fun x => projT1 x) zs))).

Inductive PureStrat {zs : Ss} {n} (p : inbounds (map (fun x => projT1 x) zs) n) (m : move (get p))
  : Type :=
| MkPureStrat {zs1 zs2} {g} (z : CoStrategy g)
    (counter : forall (cm : move g) (s : Strategy (play _ cm)),
      Action (play _ m) (fun g' =>
          map (fun x => projT1 x) (zs1 ++ existT _ (play _ cm) s :: zs2)
        = before p ++ g' :: afterl p))
.

Fixpoint mkPureStrat {zs : Ss} {n} : forall p m, PureStrat (zs := zs) (n := n) p m :=
  match zs, n with
  | [], _ => fun p => match p with end
  | existT _ g z :: zs, O => fun p m =>
    MkPureStrat (zs1 := []) (zs2 := zs) (coplay z m) (fun cm s => cm ?? eq_refl)
  | gz :: zs, S n => fun p m =>
    match mkPureStrat (zs := zs) (m := m) with
    | MkPureStrat z counter => MkPureStrat (zs := gz :: zs) (zs1 := gz :: _) z
      (fun cm s => let '(cm ?? H) := counter cm s in
        cm ?? f_equal (cons (projT1 gz)) H)
    end
  end.

Definition pureStrategy__ (zs : Ss) : Strategy (Mux_ g0 (map (fun x => projT1 x) zs)) :=
  {| coplay := fun m =>
       match m with
       | Spawn => stop (tt ?? pureStrategy_ (existT _ g0 s0 :: zs))
       | Child n p m =>
          match mkPureStrat (m := m) with
          | MkPureStrat z counter => awaiting z (fun '(cm ?? s) =>
              let '(cm ?? H) := counter cm s in Stop
              (cm ?? rw (fun zs => Strategy (Mux_ g0 zs)) H (pureStrategy_ _)))
          end
       end |}.

End Private_pureStrategy.

Definition pureStrategy_ {g0} (s0 : Strategy g0)
  : forall (zs : Ss), Strategy (Mux_ g0 (map (fun x => projT1 x) zs)) :=
  cofix _pureStrategy zs := pureStrategy__ s0 _pureStrategy zs.

End Private_pureStrategy.

Definition pureStrategy {g} (s : Strategy g) : Strategy (Mux g) :=
  Private_pureStrategy.pureStrategy_ s [].

Definition duplicateMux {g} : Strategy (Mux g *> Mux (Mux g)) :=
  apply mux (pureStrategy cloneMux).

Module Private_apMux.

Section Private_apMux.

Context {g1 g2 : Game}.

Inductive St : Type :=
| Init (_ : list (Game * Game))
| Trunk (_ _ : list (Game * Game)) (_ _ : Game)
| Fin (_ _ : list (Game * Game)) (_ _ : Game)
.

Definition OGame_ gg := fst gg *> snd gg.

Definition codom (s : St) : Game :=
  match s with
  | Init ggs =>
      Mux_ (g1 *> g2) (map OGame_ ggs) *>
      Mux_ g1 (map fst ggs) *>
      Mux_ g2 (map snd ggs)
  | Trunk beforeggs afterggs i1 i2 =>
      CoMux_ (g1 *> g2) (map OGame_ beforeggs) (map OGame_ afterggs) (i1 * i2) <*
      (Mux_ g1 (map fst beforeggs ++ i1 :: map fst afterggs) *
       CoMux_ g2 (map snd beforeggs) (map snd afterggs) i2)
  | Fin beforeggs afterggs i1 i2 =>
      Mux_ (g1 *> g2) (map OGame_ beforeggs ++ (i1 <* i2) :: map OGame_ afterggs) *>
      (CoMux_ g1 (map fst beforeggs) (map fst afterggs) i1 <*
       CoMux_ g2 (map snd beforeggs) (map snd afterggs) i2)
  end.

Context (apMux_ : forall (s : St), Strategy (codom s)).

Inductive InitChild ggs n (p : inbounds (map snd ggs) n) (m : move (get p)) : Type :=
| MkInitChild (p' : inbounds (map OGame_ ggs) n) (m' : move (get p'))
    beforeggs {afterggs i1 i2}
    (H : forall R C,
           C (map OGame_ beforeggs) (map OGame_ afterggs) (i1 * i2)
             (map fst beforeggs ++ i1 :: map fst afterggs)
             (map snd beforeggs) (map snd afterggs) i2
         = C (before p') (afterl p') (play _ m')
             (map fst ggs)
             (before p) (afterl p) (play _ m) :> R)
.

Arguments MkInitChild _ _ _ _ &.

Fixpoint mkInitChild ggs (n : nat) : forall p m, InitChild ggs n p m :=
  match ggs, n return forall p m, InitChild ggs n p m with
  | [], _ => fun p => match p in False with end
  | gg :: ggs, O => fun p m =>
    MkInitChild I m [] (afterggs := ggs) (fun _ _ => eq_refl)
  | gg :: ggs, S n => fun p m =>
    match mkInitChild (ggs := ggs) (n := n) (m := m) with
    | MkInitChild p' m' beforeggs H => MkInitChild p' m' (gg :: beforeggs)
        (fun _ C => H _ (fun x1 x2 x3 x4 x5 => C (_ :: x1) x2 x3 (_ :: x4) (_ :: x5)))
    end
  end.

Definition apMuxInit {ggs} : Strategy (codom (Init ggs)) :=
  coplaying m => stop
    match m with
    | Spawn =>
        inl Spawn ?? coplaying _ => stop@@
        inr (inl Spawn) ?? coplaying _ => stop@@
        inr (inr tt) ?? apMux_ (Init ((g1, g2) :: ggs))
    | Child n p m =>
      match mkInitChild (m := m) with
      | MkInitChild p' m' _ H =>
          inl (Child n p' m') ?? rw Strategy (H _ (fun x1 x2 x3 x4 x5 x6 x7 =>
            CoMux_ (g1 *> g2) x1 x2 x3 <* (Mux_ g1 x4 * CoMux_ g2 x5 x6 x7)))
            (apMux_ (Trunk _ _ _ _))
      end
    end.

Inductive TrunkLeft (beforeggs afterggs : list (Game * Game)) i1 (m : move i1) : Type :=
| MkTrunkLeft
    n' (p' : inbounds (map fst beforeggs ++ i1 :: map fst afterggs) n') (m' : move (get p'))
    (H : forall R C,
           C (map fst beforeggs) (map fst afterggs) (play i1 m)
         = C (before p') (afterl p') (play _ m') :> R)
.

Arguments MkTrunkLeft _ _ _ _ &.

Fixpoint mkTrunkLeft beforeggs afterggs i1 m : TrunkLeft beforeggs afterggs i1 m :=
  match beforeggs with
  | [] => MkTrunkLeft O I m (fun _ _ => eq_refl)
  | gg :: beforeggs =>
    match mkTrunkLeft with
    | MkTrunkLeft n' p' m' H => MkTrunkLeft (S n') p' m'
        (fun _ C => H _ (fun x1 => C (_ :: x1)))
    end
  end.

Inductive TrunkRight (beforeggs afterggs : list (Game * Game)) i1 i2 (m : move i2) : Type :=
| MkTrunkRight (ggs := beforeggs ++ (i1, play i2 m) :: afterggs)
    (H : forall R C,
           C (map OGame_ ggs) (map fst ggs) (map snd ggs)
         = C (map OGame_ beforeggs ++ (i1 *> play i2 m) :: map OGame_ afterggs)
             (map fst beforeggs ++ i1 :: map fst afterggs)
             (map snd beforeggs ++ play i2 m :: map snd afterggs) :> R)
.

Arguments MkTrunkRight _ _ _ _ &.

Fixpoint mkTrunkRight beforeggs afterggs i1 i2 m : TrunkRight beforeggs afterggs i1 i2 m :=
  match beforeggs with
  | [] => MkTrunkRight (fun _ _ => eq_refl)
  | gg :: beforeggs =>
    match mkTrunkRight (beforeggs := beforeggs) (m := m) with
    | MkTrunkRight H => MkTrunkRight
        (fun _ C => H _ (fun x1 x2 x3 => C (_ :: x1) (_ :: x2) (_ :: x3)))
    end
  end.

Definition apMuxTrunk {beforeggs afterggs i1 i2}
  : Strategy (codom (Trunk beforeggs afterggs i1 i2)) :=
  {| coplay := fun m => stop
       match m with
       | inl m =>
         match mkTrunkLeft with
         | MkTrunkLeft n' p' m' H =>
             inr (inl (Child n' p' m')) ?? rw Strategy
               (H _ (fun x1 x2 x3 => Mux_ _ _ *> (CoMux_ _ x1 x2 x3 <* _)))
               (apMux_ (Fin beforeggs afterggs _ i2))
         end
       | inr m =>
         match mkTrunkRight with
         | MkTrunkRight H =>
            inr (inr m) ?? rw Strategy (H _ (fun x1 x2 x3 => Mux_ _ x1 *> Mux_ _ x2 *> Mux_ _ x3))
              (apMux_ (Init _))
         end
       end
  |}.

Inductive Fini (beforeggs afterggs : list (Game * Game)) i1 i2 (m : move i1) : Type :=
| MkFini n' (p' : inbounds (map OGame_ beforeggs ++ (i1 <* i2) :: map OGame_ afterggs) n')
    (m' : move (get p')) (H : forall R C,
           C (map OGame_ beforeggs) (map OGame_ afterggs) (play i1 m * i2)
         = C (before p') (afterl p') (play _ m') :> R)
.

Arguments MkFini _ _ _ _ _ &.

Fixpoint mkFini beforeggs afterggs i1 i2 m : Fini beforeggs afterggs i1 i2 m :=
  match beforeggs with
  | [] => MkFini O I m (fun _ _ => eq_refl)
  | gg :: beforeggs =>
    match mkFini with
    | MkFini n' p' m' H => MkFini (S n') p' m'
        (fun _ C => H _ (fun x1 x2 x3 => C (_ :: x1) x2 x3))
    end
  end.

Definition apMuxFin {beforeggs afterggs i1 i2} : Strategy (codom (Fin beforeggs afterggs i1 i2)) :=
  {| coplay := fun m => stop
       match mkFini with
       | MkFini n' p' m' H =>
         inl (Child n' p' m') ?? rw Strategy (H _ (fun x1 x2 x3 => CoMux_ _ x1 x2 x3 <* _))
           (apMux_ (Trunk _ _ _ _))
       end |}.

Definition apMux__ (s : St) : Strategy (codom s) :=
  match s with
  | Init _ => apMuxInit
  | Trunk _ _ _ _ => apMuxTrunk
  | Fin _ _ _ _ => apMuxFin
  end.

End Private_apMux.

CoFixpoint apMux_ {g1 g2} (s : St) : Strategy (@codom g1 g2 s) := apMux__ apMux_ s.

End Private_apMux.

Definition apMux {g1 g2} : Strategy (Mux (g1 *> g2) *> Mux g1 *> Mux g2) :=
  Private_apMux.apMux_ (Private_apMux.Init []).
