From Coq Require Import List.
Import ListNotations.

From IOGames Require Import
  Config Util Delay Game Core Mux.

#[local] Open Scope game_scope.

Module Private_fix.

Inductive FixState : Type :=
  | FixStateL : Game -> list Game -> list Game -> Game -> FixState
  | FixStateR : Game -> list Game -> Game -> FixState
  .

Notation prefixFS := (weave OGame).
Notation suffixFS_ := (weave IGame).

Definition suffixFS (gs : list Game) (g : Game) : list Game :=
  match gs with
  | [] => []
  | g1 :: gs => suffixFS_ g1 gs ++ [last g1 gs <* g]%game
  end.

Definition codomFS (fs : FixState) : Game :=
  match fs with
  | FixStateL g0 gis gjs g =>
      CoMux_ (g0 *> g0)
                 (prefixFS g0 gis) (suffixFS gjs g) (last g0 gis * first gjs g)%game
        <* g
  | FixStateR g0 gis g =>
      Mux_ (g0 *> g0) (prefixFS g0 gis ++ [last g0 gis *> g]%game) *> g
  end.

Section Fix_.

Context (fix_0 : forall fs, Strategy (codomFS fs)).

Definition eq1_Game (g1 g2 : Game) : Prop :=
  {| play := play g1 |} = {| play := play g2 |}.

Fixpoint elem_last_weave {A} (f : A -> A -> A) (x : A) (x' : A) xs ys : inbounds (f x x' :: weave f x' xs ++ ys) (length xs) :=
  match xs with
  | [] => I
  | x'' :: xs => elem_last_weave f x' x'' xs ys
  end.

Lemma get_elem_last_weave {A B} (f : A -> A -> A) (g : A -> B) (x x' : A) xs ys
  : (forall y y', g (f y y') = g y') ->
    g (last x' xs) = g (get (elem_last_weave f x x' xs ys)).
Proof.
  intros H; revert x x'; induction xs; cbn; intros; [ symmetry; apply H | apply IHxs ].
Defined.

Section LL1.

Context {g0 g1 : Game} {gis gjs : list Game} {g : Game} (m : move (last g1 gis)).

Let p'
  : inbounds ((g0 *> g1)%game :: prefixFS g1 gis ++ (play (last g1 gis) m <* first gjs g)%game :: suffixFS gjs g) (length gis) := elem_last_weave OGame g0 g1 gis _.

Let m' : move (get p') :=
  eq_rect (move _) (fun t : Type => t) m _ (get_elem_last_weave OGame move g0 g1 gis _ (fun _ _ => eq_refl)).

Lemma LL1_rw_prefix : prefixFS g0 (init g1 gis) = before p'.
Proof.
  revert g0 g1 m p' m'; induction gis; cbn; [ reflexivity | intros; f_equal; apply IHl ].
Defined.

Lemma LL1_rw_suffix
  : suffixFS_ (play (last g1 gis) m) gjs ++ [(last (play (last g1 gis) m) gjs <* g)%game]
  = afterl p'.
Proof.
  revert g0 g1 m p' m'; induction gis; cbn; [ | intros; f_equal; apply IHl ].
  destruct gjs; cbn; reflexivity.
Defined.

Lemma LL1_rw_last
  : (last g0 (init g1 gis) * play (last g1 gis) m)%game = play (get p') m'.
Proof.
  revert g0 g1 m p' m'; induction gis; cbn; [ reflexivity | intros; f_equal; apply IHl ].
Defined.

Definition LL1_rw
  : codomFS (FixStateL g0 (init g1 gis) (play _ m :: gjs) g)
  = (CoMux_ (g0 *> g0) (before p') (afterl p') (play _ m') <* g)%game.
Proof.
  cbn [codomFS suffixFS first]; f_equal; f_equal.
  - apply LL1_rw_prefix.
  - apply LL1_rw_suffix.
  - apply LL1_rw_last.
Defined.

Definition fix_LL1
  : CoStrategy (Mux_ (g0 *> g0)
      (  (g0 *> g1)%game :: prefixFS g1 gis
      ++ (play (last g1 gis) m <* first gjs g)%game :: suffixFS gjs g) * g) :=
  stop (inl (Child _ p' m') ?? rw Strategy LL1_rw (fix_0 (FixStateL g0 _ _ _))).

End LL1.

Definition fix_LL0 {g0 gjs g} m
  : CoStrategy (Mux_ (g0 *> g0) ((play g0 m <* first gjs g)%game :: suffixFS gjs g) * g) :=
  stop (inl Spawn ?? coplaying _ => fix_LL1 (g1 := g0) (gis := []) _).

Definition fix_LL {g0 gis gjs g}
  : forall m, CoStrategy (Mux_ (g0 *> g0)
      (prefixFS g0 gis ++ (play (last g0 gis) m <* first gjs g)%game :: suffixFS gjs g) * g) :=
  match gis with
  | [] => fix_LL0
  | g1 :: gis => fix_LL1 (g1 := g1)
  end.

Definition fix_LR0 {g0 gis g} m
  : CoStrategy (Mux_ (g0 *> g0)
      (prefixFS g0 gis ++ [last g0 gis *> play g m]%game) * g) :=
  stop (inr m ?? fix_0 (FixStateR g0 gis (play g m))).

Section LR1.

Context {g0 : Game} {gis : list Game} {gj1 : Game} {gjs : list Game} {g : Game} (m : move gj1).

Let p'
  : inbounds (prefixFS g0 gis ++ (last g0 gis *> play gj1 m)%game :: suffixFS_ gj1 gjs ++ [last gj1 gjs <* g]%game) _ :=
  inbounds_app_r (n := S O) _ (match gjs with [] | _ :: _ => I end).

Let m' : move (get p') :=
  rw move (get_inbounds_app_r _ _)
    match gjs
      return move (get (n := S O) (xs := _ :: suffixFS_ _ gjs ++ [_ gjs <* _]%game)
                       (match gjs with [] | _ :: _ => I end))
    with [] | _ :: _ => m end.

Lemma LR1_rw_prefix : prefixFS g0 (gis ++ [play gj1 m]) = before p'.
Proof.
  revert g0 p' m'; induction gis; cbn.
  - destruct gjs; cbn; reflexivity.
  - intros; f_equal; apply IHl.
Defined.

Lemma LR1_rw_suffix : suffixFS gjs g = afterl p'.
Proof.
  revert g0 p' m'; induction gis; cbn.
  - destruct gjs; cbn; reflexivity.
  - intros; f_equal; apply IHl.
Defined.

Lemma LR1_rw_last : (last g0 (gis ++ [play gj1 m]) * first gjs g)%game = play (get p') m'.
Proof.
  revert g0 p' m'; induction gis; cbn.
  - destruct gjs; cbn; reflexivity.
  - intros; f_equal; apply IHl.
Defined.

Definition LR1_rw
  : codomFS (FixStateL g0 (gis ++ [play gj1 m]) gjs g)
  = (CoMux_ (g0 *> g0) (before p') (afterl p') (play _ m') <* g)%game.
Proof.
  cbn. f_equal. f_equal.
  - apply LR1_rw_prefix.
  - apply LR1_rw_suffix.
  - apply LR1_rw_last.
Defined.

Definition fix_LR1
  : CoStrategy (Mux_ (g0 *> g0)
      (  prefixFS g0 gis ++ (last g0 gis *> play gj1 m)%game
      :: suffixFS_ gj1 gjs ++ [last gj1 gjs <* g]%game) * g) :=
  stop (inl (Child _ p' m') ?? rw Strategy LR1_rw (fix_0 _)).

End LR1.

Definition fix_LR {g0 gis gjs g}
  : forall m, CoStrategy (Mux_ (g0 *> g0)
      (prefixFS g0 gis ++ (last g0 gis *> play (first gjs g) m)%game :: suffixFS gjs g) * g) :=
  match gjs with
  | [] => fix_LR0
  | gj1 :: gjs => fix_LR1 (gj1 := gj1) (gjs := gjs)
  end.

Section R.

Context {g0 : Game} {gis : list Game} {g : Game} (m : move g).

Let p' : inbounds (prefixFS g0 gis ++ [last g0 gis *> g]%game) (length _ + 0) := inbounds_app_r (n := O) _ I.

Let m' : move (get p') :=
  rw move (get_inbounds_app_r O (xs := prefixFS g0 gis) (ys := [_]%game) I) (m : move (_ *> g)).

Lemma R_rw_prefix : prefixFS g0 gis = before p'.
Proof.
  revert g0 p' m'; induction gis; cbn; [ reflexivity | intros; f_equal; apply IHl ].
Defined.

Lemma R_rw_suffix : [] = afterl p'.
Proof.
  revert g0 p' m'; induction gis; cbn; [ reflexivity | intros; f_equal; apply IHl ].
Defined.

Lemma R_rw_last : (last g0 gis * play g m)%game = play (get p') m'.
Proof.
  revert g0 p' m'; induction gis; cbn; [ reflexivity | intros; f_equal; apply IHl ].
Defined.

Definition R_rw
  : codomFS (FixStateL g0 gis [] (play g m))
  = (CoMux_ (g0 *> g0) (before p') (afterl p') (play _ m') <* play g m)%game.
Proof.
  cbn. f_equal. f_equal.
  - apply R_rw_prefix.
  - apply R_rw_suffix.
  - apply R_rw_last.
Defined.

Definition fix_R
  : CoStrategy (Mux_ (g0 *> g0) (prefixFS g0 gis ++ [last g0 gis *> g]%game) * play g m) :=
  stop (inl (Child _ p' m') ?? rw Strategy R_rw (fix_0 _)).

End R.

Arguments fix_R _ _ _ &.

Definition fix__ fs : Strategy (codomFS fs) :=
  match fs with
  | FixStateL _ _ _ _ => coplaying m =>
      match m with
      | inl m => fix_LL m
      | inr m => fix_LR m
      end
  | FixStateR _ _ _ => coplaying m => fix_R m
  end.

End Fix_.

CoFixpoint fix_ fs : Strategy (codomFS fs) :=
  fix__ fix_ fs.

End Private_fix.

(* !(g -o g) -o g *)
Definition fix_ {g} : Strategy (Mux (g *> g) *> g) :=
  coplaying m => stop@@
     inl Spawn ?? coplaying _ => stop@@
     inl (Child O I m) ?? Private_fix.fix_ (Private_fix.FixStateL g [] [] (play g m)).

(*
  F(g,i,j) = C(g,i,j) *> (gi * g(i+1))
where
  C(g,i,j) = !(g0 *> g0)
      * ((g0 *> g1) * ... * (g(i-1) *> gi))
      * ((g(i+1) <* g(i+2)) * ... * (g(i+j) <* g(i+j-1)))

Left step, for i>0
  F(g,i,j)
  =
  C(g,i,j) *> (gi * g(i+1))
         mi \
             C(g,i,j) * (play gi mi <* g(i+1)))
             =
             C(g[i -> play gi mi],i-1,j+1) * (g(i-1) *> gi)
         mi /
  C(g[i -> play gi mi],i-1,j+1) *> (g(i-1) * play gi mi)
  =
  F(g[i -> play gi mi],i-1,j+1)

Left step, for i=0
  TODO

Right step, for j>0
  F(g,i,j)
  =
  C(g,i,j) *> (gi * g(i+1))
        mi' \
             C(g,i,j) * (gi *> play g(i+1) mi')
             =
             C(g[i+1 -> play g(i+1) mi'],i+1,j-1) * (g(i+1) <* g(i+2))
        mi' /
  C(g[i+1 -> play g(i+1) mi'],i+1,j-1) *> (play g(i+1) mi' * g(i+2))
  =
  F(g[i+1 -> play g(i+1) mi'],i+1,j-1)

Right step, for j=0
  TODO
*)

