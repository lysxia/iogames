From Coq Require Import NArith.
From IOGames Require Import Config Delay.

Definition force {A} (c : delay A) (n : N) : option (A * N) :=
  N.recursion (fun _ => None) (fun n k (c : delay A) =>
    match await c with
    | Step c => k c
    | Stop y => Some (y, n)
    end) n c.
