(** * Linear lambda calculus *)

From Coq Require Import List Arith.
Import ListNotations.

From IOGames Require Import Config Util.

Inductive ty (A : Type) : Type :=
| TyBase : A -> ty A
| TyArr : ty A -> ty A -> ty A
.

Definition ctx (A : Type) : Type := list (nat * ty A).

(* We use [tt = tt] instead of [True] so we can just write [eq_refl] in all cases. *)
Definition head_ {A} (g : ctx A) (n : nat) (t : ty A) : Prop :=
  match g with
  | (n', t') :: _ => if Nat.eqb n n' then t = t' else tt = tt
  | [] => tt = tt
  end.

Definition tail_ {A} (g : ctx A) (n : nat) : ctx A :=
  match g with
  | (n', _) :: g' => if Nat.eqb n n' then g' else g
  | [] => []
  end.

Inductive interleaving {A} : list A -> list A -> list A -> Type :=
| IL_nil : interleaving [] [] []
| IL_L x xs1 xs2 xs3 : interleaving xs1 xs2 xs3 -> interleaving (x :: xs1) xs2 (x :: xs3)
| IL_R x xs1 xs2 xs3 : interleaving xs1 xs2 xs3 -> interleaving xs1 (x :: xs2) (x :: xs3)
.

Class Splitter {A} (g1 g2 g : ctx A) : Type :=
  splitter : interleaving g1 g2 g.

#[export] Instance Splitter_nil {A} : Splitter (A := A) [] [] [] :=
  IL_nil.

Class Equal {A} (x y : A) : Prop :=
  equal : x = y.

#[export] Instance Equal_refl {A} (x : A) : Equal x x := eq_refl.

#[export] Instance Splitter_split_L {A} n1 t1 n2 t2 (g1 g2 g : ctx A)
    `(!Equal (Nat.ltb n2 n1) true) `(!Splitter g1 ((n2, t2) :: g2) g)
  : Splitter ((n1, t1) :: g1) ((n2, t2) :: g2) ((n1, t1) :: g) :=
  IL_L splitter.

#[export] Instance Splitter_split_R {A} n1 t1 n2 t2 (g1 g2 g : ctx A)
    `(!Equal (Nat.ltb n1 n2) true) `(!Splitter ((n1, t1) :: g1) g2 g)
  : Splitter ((n1, t1) :: g1) ((n2, t2) :: g2) ((n2, t2) :: g) :=
  IL_R splitter.

#[export] Instance Splitter_split_L_nil {A} g
  : Splitter (A := A) g [] g.
Admitted.

#[export] Instance Splitter_split_R_nil {A} g
  : Splitter (A := A) [] g g.
Admitted.

(* Invariant: context sorted with decreasing levels *)
Inductive term {A} (l : nat) : ctx A -> ty A -> Type :=
| Var n t : term l [(n, t)] t
| Lam g t u : term (S l) ((l, t) :: g) u -> term l g (TyArr t u)
| NoLam g t u : term (S l) g u -> term l g (TyArr t u)  (* Unused variable *)
| App g g1 g2 t1 t2 : term l g1 t1 -> term l g2 (TyArr t1 t2) -> Splitter g1 g2 g -> term l g t2
.

Inductive var {A : Type} (n : nat) (t : ty A) : Type := V.

Definition lam_ {A l g} {t u : ty A}
    (f : var l t -> term (S l) ((l, t) :: g) u) : term l g (TyArr t u) :=
  Lam (f V).

Arguments lam_ _ _ _ _ &.

Definition v {A l} {n} {t : ty A} (_ : var n t) : term l [(n, t)] t := Var.

Arguments v _ _ &.

Notation "'lam' x ':' t '=>' y" := (lam_ (t := t) (fun x => y))
  (at level 0, x binder, x at level 0, t at level 0, y at level 200).

Notation "x '<|' f" := (App x f _) (at level 40).

Definition ffff : term (A := unit) 0 [] _ :=
  lam x : TyBase tt => v x.

Definition fgggg : term (A := unit) 0 [] _ :=
  lam f : TyArr (TyBase tt) (TyBase tt) =>
  lam x : TyBase tt =>
  v x <| v f.
